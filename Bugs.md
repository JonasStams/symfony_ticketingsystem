# views/admins/index.php #

- **Lijn nr: 115 =>** $this->load->library('table'); **=> kan beter in controller gezet worden (MVC principe)**
- **Lijn nr: 88,95 => ** p-tag met style="height:35px" **=> stijl in css plaatsen**
- **Lijn nr: 245 =>** h4-tag met style="font-size:1.6em;" **=> stijl in css plaatsen**

# views/members/members
- Overzicht tabel => description en subject omgedraaid
- Modal wijzig ticket: geen lijst voor campussen, geen text area voor beschrijving, beschrijving en onderwerp zijn omgedraaid
- **Lijn nr: 107 =>** p-tag met style="height:35px" **=> stijl in css plaatsen**
- **Lijn nr: 114 =>** p-tag met style="height:35px" **=> stijl in css plaatsen**
- **Lijn nr: 269 =>** h4-tag met style="font-size:1.6em;" **=> stijl in css plaatsen**
- **Lijn nr: 271 =>** div-tag met style="padding:40px 50px; background-color:white" **=> stijl in css plaatsen**

# controllers/Members
- Bij het aanmaken van een ticket: Als enkele velden niet word ingevuld, dan verschijnt er gewoon een wit scherm. Denk dat de fout in de create_ticket methode zit.
- Bij het wijzigen van een ticket: als je het modal sluit zonder op opslaan te klikken verschijnt onder de tabel pagination. In de adresbalk van de browser zie je dan ook dat deze zich nog in de ticket_table_hander methode zit. Als je gewoon op opslaan klikt dan gaat het wel goed.

# views/dispatchers/dispatchers

- **Lijn nr: 70 =>** ul-tag met style="margin-top: 10px;" **=> stijl in css plaatsen**
- **Lijn nr: 82 =>** p-tag met style="height:35px" **=> stijl in css plaatsen**
- **Lijn nr: 144 =>** h4-tag met style="font-size:1.6em;" **=> stijl in css plaatsen**
- **Lijn nr: 175 =>** p-tag met style="height:35px" **=> stijl in css plaatsen**



# controllers/Dispatchers
- Als je meerdere tickets selecteerd op de dispatcher pagina en je klikt op de knop meer info, dan komt er een 404 pagina.
- Als je na het opvragen van meer info van een ticket naar de werkmannen tab gaat dan is er een php error. Door het opvragen van meer info word de ticket_table_hanlder methode opgeroepen. Ook als je het modal sluit staat deze nog in de adresbalk, hierdoor ontstaat de fout.

# views/workers/workers
- **Lijn nr: 101 =>** p-tag met style="height:35px" **=> stijl in css plaatsen**
- **Lijn nr: 103 - 121 =>** buttons met style="margin-bottom:25px;" **=> stijl in css plaatsen**
- **Lijn nr: 178 =>** div-tag met style="text-align: center;" **=> stijl in css plaatsen**
- **Lijn nr: 181 =>** h4-tag met style="font-size:1.6em;" **=> stijl in css plaatsen**
- **Lijn nr: 183 =>** div-tag met style="padding:40px 50px; background-color:white" **=> stijl in css plaatsen**
- **Lijn nr: 219 =>** h4-tag met style="font-size:1.6em;" **=> stijl in css plaatsen**
- **Lijn nr: 221 =>** div-tag met style="padding:40px 50px; background-color:white" **=> stijl in css plaatsen**
- **Lijn nr: 267 =>** form_submit met 'change_pw'. Als knop tekst stond ook nog verander wachtwoord. **=> change_pw veranderen!**

# controllers/Workers
- Op het worker panel: bij het klikken op de knop "Nieuw bericht" zonder op voorhand een ticket te selecteren. Je word dan terug naar het user panel gestuurd.
- Op het worker panel: selecteer ticket en klik op knop "controleer" berichten. Er verschijnt een PHP error.
- Op het worker panel: geen ticket selecteren en dan op één van de drie knoppen klikken om de status te veranderen. Verschijnt een witte pagina.
- Iets wat mij is opgevallen en wat ik getest heb. Tickets die niet toegekend zijn, dus waarvan assigned_to NULL is kunnen gezien worden door alle werkmannen. Enkel dispatchers zouden dit mogen zien. Werkmannen mogen enkel de ticekts zien die aan hun zijn toegewezen.