<?php
/**
 * Created by PhpStorm.
 * @Author: Koen Castermans
 * @Reviewer:
 * Date: 5/05/2016
 * Time: 17:51
 */

// $lang[''] = '';
// echo $this->lang->line('');

/*
 * Gemeenschappelijk
 */
$lang['user_panel'] = 'Ga naar gebruikers pagina';
$lang['signed_in'] = 'Ingelogd als ';
$lang['logout'] = 'Afmelden';
$lang['system_name'] = 'Ticketing systeem';
$lang['search_ticket'] = 'Zoek ticket';
$lang['ticket_table_ticketid'] = 'TicketId';
$lang['ticket_table_campus'] = 'Campus';
$lang['ticket_table_place'] = 'Plaats';
$lang['ticket_table_subject'] = 'Onderwerp';
$lang['ticket_table_description'] = 'Beschrijving';
$lang['ticket_table_status'] = 'Status';
$lang['ticket_table_created'] = 'Gemaakt op';
$lang['user_table_id'] = 'Id';
$lang['user_table_username'] = 'Gebruikersnaam';
$lang['user_table_email'] = 'Email';
$lang['user_table_role'] = 'Rol';
$lang['user_table_status'] = 'Status';

/*
restricted pagina
 */
$lang['error_title'] = 'Errors - PXL Ticketing systeem';
$lang['error_h1_no_access'] = 'U hebt geen toegang tot deze pagina';
$lang['error_back'] = 'Terug naar login';

/*
views/login
*/
$lang['login_title'] = 'Login - PXL ticketing systeem';
$lang['login_form_h2'] = 'Log In';
$lang['login_form_label_email'] = 'Email';
$lang['login_form_label_wachtwoord'] = 'Wachtwoord';
$lang['login_form_submit'] = 'Login';
$lang['main_message_acc_blocked'] = 'Uw account is geblokkeerd, neem contact op met de systeembeheerder.'; //message die in de main controller word doorgegeven aan login view
$lang['main_message_incorrect_email_password'] = 'Fout email/wachtwoord.';

/*
views/admins/index
*/
$lang['admins_index_title'] = 'Admin pagina - PXL ticketing systeem';
$lang['admins_index_h3_users'] = 'Gebruikers';
$lang['admins_index_info_users'] = 'Hier kunnen administrators de gebruikeraccounts aanpassen.';
$lang['admins_index_h4_new_user'] = 'Maak een nieuwe gebruiker aan';
$lang['admins_index_info_new_user'] = 'Hier kan u een nieuw gebruikersaccount aanmaken.';
$lang['admins_index_h4_check_users'] = 'Beheer gebruikers';
$lang['admins_index_info_check_users'] = 'Hier kan u alle accounts bekijken en aanpassen.';
$lang['admins_index_button_add'] = 'Toevoegen';
$lang['admins_index_button_(un)block'] = '(De)blokkeren';
$lang['admins_index_button_delete'] = 'Verwijderen';
$lang['admins_index_button_modify'] = 'Aanpassen';
$lang['admins_index_modal_h4_create_user'] = 'Maak een gebruiker';
$lang['admins_index_modal_label_email'] = 'Email';
$lang['admins_index_modal_label_username'] = 'Gebruikersnaam';
$lang['admins_index_modal_label_password'] = 'Wachtwoord';
$lang['admins_index_modal_label_password_confirm'] = 'Bevestig Wachtwoord';
$lang['admins_index_modal_label_role'] = 'Soort gebruiker';
$lang['admins_index_modal_submit_save'] = 'Opslaan';
$lang['admins_index_modal_submit_save_new'] = 'Opslaan en voeg nieuwe toe';
$lang['admins_index_modal_h4_modify_user'] = 'Gebruiker aanpassen';
$lang['admins_index_modal_h5_modify_password'] = 'Wachtwoord veranderen';
$lang['admins_index_modal_label_new_password'] = 'Nieuw wachtwoord';
$lang['admins_index_modal_label_new_password_confirm'] = 'Bevestig wachtwoord';
$lang['admins_index_modal_submit_change'] = 'Verander wachtwoord';
$lang['admins_index_mention_delete'] = 'Bent u zeker? Een gebruiker verwijderen kan niet ongedaan gemaakt worden.';
//messages en errors van de admins controller
$lang['controller_admins_message_user_added'] = 'De gebruiker is toegevoegd.';
$lang['controller_admins_message_user_deleted'] = 'De gebruiker is verwijderd.';
$lang['controller_admins_message_multiple_users_deleted'] = 'De gebruikers zijn verwijderd.';
$lang['controller_admins_message_user_(un)blocked'] = 'Gebruikers zijn ge(de)blokkeerd.';
$lang['controller_admins_message_password_changed'] = 'Het wachtwoord is aangepast.';
$lang['controller_admins_error_user_not_selected'] = 'U heeft geen gebruiker geselecteerd.';
$lang['controller_admins_error_modify_one_user'] = 'U kan maar één user per keer aanpassen.';
$lang['controller_admins_error_select_user'] = 'U moet een user selecteren.';
$lang['controller_admins_error_no_identical_pass'] = 'U heeft geen identieke wachtwoorden ingegeven.';
$lang['controller_admins_error_already_used'] = 'Gebruikersnaam en/of email adres is al in gebruik.';
$lang['controller_admins_error_different_passwords'] = 'U gaf 2 verschillende wachtwoorden in.';

/*
 * views/members/members
 */
$lang['view_members_title'] = 'Members pagina - PXL Ticketing systeem';
$lang['view_members_admin_panel'] = 'Ga naar Admin pagina';
$lang['view_members_dispatcher_panel'] = 'Ga naar Dispatchers pagina';
$lang['view_members_worker_panel'] = 'Ga naar Workers pagina';
$lang['view_members_h3_tickets'] = 'Tickets';
$lang['view_members_info_tickets'] = 'Hier kan je tickets wijzigen of toevoegen.';
$lang['view_members_search_placeholder'] = 'Zoek ticket';
$lang['view_members_h4_create_ticket'] = 'Maak een nieuw ticket';
$lang['view_members_info_create_ticket'] = 'Hier kan je een nieuw ticket toevoegen.';
$lang['view_members_button_add_ticket'] = 'Toevoegen';
$lang['view_members_h4_edit_tickets'] = 'Wijzig tickets';
$lang['view_members_info_edit_tickets'] = 'Hier kan je jouw tickets aanpassen of verwijderen.';
$lang['view_members_button_delete_ticket'] = 'Verwijder';
$lang['view_members_button_mention_delete'] = 'Het ticket zal definitief verwijderd worden, bent u zeker?';
$lang['view_members_button_modify_ticket'] = 'Wijzig';
$lang['view_members_modal_h4_create_ticket'] = 'Maak ticket';
$lang['view_members_modal_label_campus'] = 'Campus';
$lang['view_members_modal_label_place'] = 'Plaats';
$lang['view_members_modal_label_subject'] = 'Onderwerp';
$lang['view_members_modal_label_description'] = 'Beschrijving';
$lang['view_members_modal_create_submit_save'] = 'Opslaan';
$lang['view_members_modal_create_submit_save_new'] = 'Opslaan en voeg nieuwe toe';
$lang['view_members_modal_modify_h4'] = 'Wijzig ticket';
$lang['view_members_modal_modify_h5'] = 'Wijzig de velden';
$lang['view_members_modal_modify_submit_save'] = 'Wijzigingen opslaan';
$lang['controller_members_message_ticket_created'] = 'Uw ticket is aangemaakt.';
$lang['controller_members_message_ticket_created_fail'] = 'Er is een fout opgetreden bij het aanmaken van uw ticket. Probeer het opnieuw.';
$lang['controller_members_message_ticket_updated'] = 'Het ticket is gewijzigd.';
$lang['controller_members_message_ticket_deleted'] = 'Het ticket is verwijderd.';
$lang['controller_members_error_delete_select'] = 'U heeft geen ticket geselecteerd om te verwijderen.';
$lang['controller_members_error_edit_one_ticket'] = 'U kan maar 1 ticket per keer wijzigen. Pas uw selectie aan.';
$lang['controller_members_error_edit_no_selection'] = 'U moet een ticket selecteren om deze te kunnen wijzigen.';

/*
 * views/dispatchers/dispatchers
 */
$lang['view_dispatchers_title']='Dispatcher pagina - PXL ticketing systeem';
$lang['view_dispatchers_tab1_name']='Open tickets';
$lang['view_dispatchers_tab2_name']='Werkmannen';
$lang['view_dispatchers_h4_open_tickets']='Alle open tickets';
$lang['view_dispatchers_info_open_tickets']='Hier kan u de tickets onder de werkmannen verdelen.';
$lang['view_dispatchers_button_assign_ticket']='Ticket toewijzen';
$lang['view_dispatchers_button_delete']='Verwijderen';
$lang['view_dispatchers_button_status']='Wijzig status';
$lang['view_dispatchers_button_info']='Meer info';
$lang['view_dispatchers_mention_delete']='Het ticket zal definitief verwijderd worden, bent u zeker?';
$lang['view_dispatchers_modal_info_h4_overview']='Ticket overzicht:';
$lang['view_dispatchers_modal_info_id']='Ticket id: ';
$lang['view_dispatchers_modal_info_submitted']='Ingediend door: ';
$lang['view_dispatchers_modal_info_worker']='Werkman: ';
$lang['view_dispatchers_modal_info_status']='Status: ';
$lang['view_dispatchers_modal_info_subject']='Onderwerp: ';
$lang['view_dispatchers_modal_info_description']='Beschrijving: ';
$lang['view_dispatchers_modal_info_campus']='Campus: ';
$lang['view_dispatchers_modal_info_place']='Plaats: ';
$lang['view_dispatchers_modal_info_priority']='Prioriteit: ';
$lang['view_dispatchers_modal_info_creation_date']='Aanmaakdatum: ';
$lang['view_dispatchers_h4_available_workers'] = 'Alle beschikbare werkmannen';
$lang['view_dispatchers_info_available_workers'] = 'Kies hier een werkman die het ticket moet behandelen.';
$lang['view_dispatchers_button_assign_worker'] = 'Werkman toewijzen';
$lang['view_dispatchers_modal_info_choose_worker'] ='Kies werkman: ';
$lang['controller_dispatchers_message_ticket_updated'] = 'Het ticket is gewijzigd.';
$lang['controller_dispatchers_message_ticket_deleted'] = 'Het ticket is verwijderd.';
$lang['view_dispatchers_modal_info_h4_asign_worker']='Kies Werkman';
$lang['worker_table_id']='Id';
$lang['worker_table_username']='Gebruikersnaam';
$lang['worker_table_email']='Email';
$lang['worker_table_role']='Rol';
$lang['worker_table_status']='Status';

/*
 * views/workers/workers
 */
$lang['view_workers_title'] = 'Werkmannen pagina - PXL ticketing systeem';
$lang['view_workers_h3_tickets'] = 'Tickets';
$lang['view_workers_info_ticket_status'] = 'Uw tickets volgens status.';
$lang['view_workers_status_awaiting'] = 'In afwachting';
$lang['view_workers_status_in_progress'] = 'In behandeling';
$lang['view_workers_status_on_hold'] = 'In de wacht';
$lang['view_workers_status_solved'] = 'Opgelost';
$lang['view_workers_status_closed'] = 'Afgesloten';
$lang['view_workers_info_change_status'] = 'Wijzig de ticket status of maak een nieuw ticket bericht.';
$lang['view_workers_button_new_message'] = 'Nieuw bericht';
$lang['view_workers_button_check_messages'] = 'Bekijk berichten';
$lang['view_workers_button_set_in_progress'] = 'Zet In Behandeling';
$lang['view_workers_button_set_on_hold'] = 'Zet In De Wacht';
$lang['view_workers_button_set_closed'] = 'Zet Afgesloten';
$lang['view_workers_warning'] = 'Opgelet!';
$lang['view_workers_warning_there_are'] = ' Er zijn ';
$lang['view_workers_warning_ticket_approvement'] = ' tickets die wachten op uw goedkeuring.';
$lang['view_workers_modal_check_h4'] = 'Bekijk berichten';
$lang['view_workers_modal_check&create_id'] = 'Ticket #';
$lang['view_workers_modal_check&create_description'] = 'Beschrijving: ';
$lang['view_workers_modal_check_message_nr'] = 'Bericht #';
$lang['view_workers_modal_check_from'] = 'Van: ';
$lang['view_workers_modal_check_created'] = 'Aangemaakt op: ';
$lang['view_workers_modal_check_message'] = 'Bericht: ';
$lang['view_workers_modal_create_h4'] = 'Maak een nieuw bericht';
$lang['view_workers_modal_create_label_message'] = 'Bericht';
$lang['view_workers_modal_create_submit_create'] = 'Maak bericht';
$lang['controller_workers_message_new_message_correct'] = 'Nieuw bericht is succesvol aangemaakt.';
$lang['controller_workers_error_new_message_fail'] = 'Het maken van een nieuw bericht is niet gelukt, probeer opnieuw.';
$lang['controller_workers_error_edit_one_ticket'] = 'U kan maar één ticket per keer aanpassen, pas uw selectie aan.';
$lang['controller_workers_error_select_ticket'] = 'U moet een ticket selecteren om deze optie te kunnen gebruiken.';
$lang['ticket_table_worker_id'] = 'WerkmanId';



