<?php
/**
 * Created by PhpStorm.
 * @Author: Koen Castermans
 * @Reviewer:
 * Date: 10/05/2016
 * Time: 22:16
 */

// $lang[''] = '';
// echo $this->lang->line('');

/*
 * Gemeenschappelijk
 */
$lang['user_panel'] = 'Go to user page.';
$lang['signed_in'] = 'Signed in as ';
$lang['logout'] = 'Log out';
$lang['system_name'] = 'Ticketing system';
$lang['search_ticket'] = 'search ticket';
$lang['ticket_table_ticketid'] = 'TicketId';
$lang['ticket_table_campus'] = 'Campus';
$lang['ticket_table_place'] = 'Place';
$lang['ticket_table_subject'] = 'Subject';
$lang['ticket_table_description'] = 'Description';
$lang['ticket_table_status'] = 'Status';
$lang['ticket_table_created'] = 'Created on';
$lang['user_table_id'] = 'Id';
$lang['user_table_username'] = 'Username';
$lang['user_table_email'] = 'Email';
$lang['user_table_role'] = 'Role';
$lang['user_table_status'] = 'Status';

/*
restricted pagina
 */
$lang['error_title'] = 'Errors - PXL Ticketing system';
$lang['error_h1_no_access'] = "U don't have access to this page!";
$lang['error_back'] = 'Back to login.';

/*
views/login
*/
$lang['login_title'] = 'Login - PXL ticketing system';
$lang['login_form_h2'] = 'Log In';
$lang['login_form_label_email'] = 'Email';
$lang['login_form_label_wachtwoord'] = 'Password';
$lang['login_form_submit'] = 'Login';
$lang['main_message_acc_blocked'] = 'Your account is blocked, contact the system administrator.'; //message die in de main controller word doorgegeven aan login view
$lang['main_message_incorrect_email_password'] = 'Incorrect email/password.';

/*
views/admins/index
*/
$lang['admins_index_title'] = 'Admin page - PXL ticketing system';
$lang['admins_index_h3_users'] = 'Users';
$lang['admins_index_info_users'] = 'Administrators can modify the user accounts.';
$lang['admins_index_h4_new_user'] = 'Create a new user';
$lang['admins_index_info_new_user'] = 'Here you can make a new user account.';
$lang['admins_index_h4_check_users'] = 'Manage users';
$lang['admins_index_info_check_users'] = 'Here you can look at the accounts and modify them.';
$lang['admins_index_button_add'] = 'Add';
$lang['admins_index_button_(un)block'] = '(un)block';
$lang['admins_index_button_delete'] = 'Delete';
$lang['admins_index_button_modify'] = 'Modify';
$lang['admins_index_modal_h4_create_user'] = 'Create a user';
$lang['admins_index_modal_label_email'] = 'Email';
$lang['admins_index_modal_label_username'] = 'Username';
$lang['admins_index_modal_label_password'] = 'Password';
$lang['admins_index_modal_label_password_confirm'] = 'Confirm password';
$lang['admins_index_modal_label_role'] = 'User role';
$lang['admins_index_modal_submit_save'] = 'Save';
$lang['admins_index_modal_submit_save_new'] = 'Save and add new';
$lang['admins_index_modal_h4_modify_user'] = 'Modify user';
$lang['admins_index_modal_h5_modify_password'] = 'Change password';
$lang['admins_index_modal_label_new_password'] = 'New password';
$lang['admins_index_modal_label_new_password_confirm'] = 'Confirm password';
$lang['admins_index_modal_submit_change'] = 'Change password';
$lang['admins_index_mention_delete'] = 'Are u sure? Deleting a user is irreversible.';
//messages en errors van de admins controller
$lang['controller_admins_message_user_added'] = 'The user has been added.';
$lang['controller_admins_message_user_deleted'] = 'The user has been deleted.';
$lang['controller_admins_message_multiple_users_deleted'] = 'The users are deleted.';
$lang['controller_admins_message_user_(un)blocked'] = 'Users are (un)blocked.';
$lang['controller_admins_message_password_changed'] = 'The password has been changed.';
$lang['controller_admins_error_user_not_selected'] = "U didn't select a user.";
$lang['controller_admins_error_modify_one_user'] = 'U can only change one user at a time. Change your selection.';
$lang['controller_admins_error_select_user'] = 'U have to select a user.';
$lang['controller_admins_error_no_identical_pass'] = 'The passwords are not identical.';
$lang['controller_admins_error_already_used'] = 'Username and/or email address are already in use.';
$lang['controller_admins_error_different_passwords'] = 'The passwords are not identical.';

/*
 * views/members/members
 */
$lang['view_members_title'] = 'Members page- PXL Ticketing system';
$lang['view_members_admin_panel'] = 'Go to Admin page';
$lang['view_members_dispatcher_panel'] = 'Go to Dispatchers page';
$lang['view_members_worker_panel'] = 'Go to Workers page';
$lang['view_members_h3_tickets'] = 'Tickets';
$lang['view_members_info_tickets'] = 'Here you can add or modify tickets.';
$lang['view_members_search_placeholder'] = 'Search ticket';
$lang['view_members_h4_create_ticket'] = 'Create a new ticket';
$lang['view_members_info_create_ticket'] = 'Here you can add a new ticket.';
$lang['view_members_button_add_ticket'] = 'Add';
$lang['view_members_h4_edit_tickets'] = 'Modify tickets';
$lang['view_members_info_edit_tickets'] = 'Here you can modify or delete your tickets.';
$lang['view_members_button_delete_ticket'] = 'Delete';
$lang['view_members_button_mention_delete'] = 'The ticket will be deleted permanently. Are you sure?';
$lang['view_members_button_modify_ticket'] = 'Modify';
$lang['view_members_modal_h4_create_ticket'] = 'Create ticket';
$lang['view_members_modal_label_campus'] = 'Campus';
$lang['view_members_modal_label_place'] = 'Place';
$lang['view_members_modal_label_subject'] = 'Subject';
$lang['view_members_modal_label_description'] = 'Description';
$lang['view_members_modal_create_submit_save'] = 'Save';
$lang['view_members_modal_create_submit_save_new'] = 'Save and add new';
$lang['view_members_modal_modify_h4'] = 'Modify ticket';
$lang['view_members_modal_modify_h5'] = 'Modify the fields';
$lang['view_members_modal_modify_submit_save'] = 'Save changes';
$lang['controller_members_message_ticket_created'] = 'Your ticket has been created.';
$lang['controller_members_message_ticket_created_fail'] = 'Error, your ticket could not be created. Please try again.';
$lang['controller_members_message_ticket_updated'] = 'The tickets has been modified.';
$lang['controller_members_message_ticket_deleted'] = 'The ticket has been deleted.';
$lang['controller_members_error_delete_select'] = 'U did not select a ticket to delete.';
$lang['controller_members_error_edit_one_ticket'] = 'U can only modify one ticket at a time. Change your selection.';
$lang['controller_members_error_edit_no_selection'] = 'U have to select a ticket before you can modify one.';

/*
 * views/dispatchers/dispatchers
 */
$lang['view_dispatchers_title']='Dispatcher page - PXL ticketing system';
$lang['view_dispatchers_tab1_name']='Open tickets';
$lang['view_dispatchers_tab2_name']='Workers';
$lang['view_dispatchers_h4_open_tickets']='All open tickets';
$lang['view_dispatchers_info_open_tickets']='Here you distribute the tickets between the workers.';
$lang['view_dispatchers_button_assign_ticket']='Assign ticket';
$lang['view_dispatchers_button_delete']='Delete';
$lang['view_dispatchers_button_status']='Modify status';
$lang['view_dispatchers_button_info']='More info';
$lang['view_dispatchers_mention_delete']='The ticket will be deleted permanently. Are you sure?';
$lang['view_dispatchers_modal_info_h4_overview']='Ticket summary:';
$lang['view_dispatchers_modal_info_id']='Ticket id: ';
$lang['view_dispatchers_modal_info_submitted']='Submitted by: ';
$lang['view_dispatchers_modal_info_worker']='Worker: ';
$lang['view_dispatchers_modal_info_status']='Status: ';
$lang['view_dispatchers_modal_info_subject']='Subject: ';
$lang['view_dispatchers_modal_info_description']='Description: ';
$lang['view_dispatchers_modal_info_campus']='Campus: ';
$lang['view_dispatchers_modal_info_place']='Place: ';
$lang['view_dispatchers_modal_info_priority']='Priority: ';
$lang['view_dispatchers_modal_info_creation_date']='Creation date: ';
$lang['view_dispatchers_h4_available_workers'] = 'All available wokers';
$lang['view_dispatchers_info_available_workers'] = 'Choose a worker who can solve the ticket.';
$lang['view_dispatchers_button_assign_worker'] = 'Assign worker';
$lang['view_dispatchers_modal_info_choose_worker'] ='Choose worker: ';
$lang['controller_dispatchers_message_ticket_updated'] = 'The ticket has been modified.';
$lang['controller_dispatchers_message_ticket_deleted'] = 'The ticket has been deleted.';
$lang['view_dispatchers_modal_info_h4_asign_worker']='Asign Worker';
$lang['worker_table_id']='Id';
$lang['worker_table_username']='Username';
$lang['worker_table_email']='Email';
$lang['worker_table_role']='Role';
$lang['worker_table_status']='Status';

/*
 * views/workers/workers
 */
$lang['view_workers_title'] = 'Workers page - PXL ticketing system';
$lang['view_workers_h3_tickets'] = 'Tickets';
$lang['view_workers_info_ticket_status'] = 'Your tickets by status:';
$lang['view_workers_status_awaiting'] = 'Pending';
$lang['view_workers_status_in_progress'] = 'Being treated';
$lang['view_workers_status_on_hold'] = 'On hold';
$lang['view_workers_status_solved'] = 'Solved';
$lang['view_workers_status_closed'] = 'Closed';
$lang['view_workers_info_change_status'] = 'Change the status of the ticket or create a new message.';
$lang['view_workers_button_new_message'] = 'New message';
$lang['view_workers_button_check_messages'] = 'View messages';
$lang['view_workers_button_set_in_progress'] = 'Set Being treated';
$lang['view_workers_button_set_on_hold'] = 'Set On Hold';
$lang['view_workers_button_set_closed'] = 'Set Closed';
$lang['view_workers_warning'] = 'Warning!';
$lang['view_workers_warning_there_are'] = ' There are ';
$lang['view_workers_warning_ticket_approvement'] = ' tickets waiting for your approval.';
$lang['view_workers_modal_check_h4'] = 'View messages';
$lang['view_workers_modal_check&create_id'] = 'Ticket #';
$lang['view_workers_modal_check&create_description'] = 'Description: ';
$lang['view_workers_modal_check_message_nr'] = 'Message #';
$lang['view_workers_modal_check_from'] = 'From: ';
$lang['view_workers_modal_check_created'] = 'Created on: ';
$lang['view_workers_modal_check_message'] = 'Message: ';
$lang['view_workers_modal_create_h4'] = 'Create a new message';
$lang['view_workers_modal_create_label_message'] = 'Message';
$lang['view_workers_modal_create_submit_create'] = 'Create message';
$lang['controller_workers_message_new_message_correct'] = 'The new message has been created.';
$lang['controller_workers_error_new_message_fail'] = 'The creation of the message has failed. Try again.';
$lang['controller_workers_error_edit_one_ticket'] = 'U can only modify one ticket at a time. Change your selection.';
$lang['controller_workers_error_select_ticket'] = 'U have to select a ticket to use this option.';
$lang['ticket_table_worker_id'] = 'WorkerId';


