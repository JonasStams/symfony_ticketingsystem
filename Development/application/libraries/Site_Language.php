<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * @Author: Koen Castermans
 * Date: 14/05/2016
 * Time: 15:50
 * @Info: Klasse die helpt bij het laden van de juiste taal.
 */
class Site_Language
{
    protected $CI;//property voor CI super object

    public function __construct()
    {
        // =& -> is 'pass by reference' dit zorgt ervoor dat altijd het originele CI super object gebruikt word ipv een kopie.
        $this->CI =& get_instance();//Codeigniter super object in een variabele zetten

    }

    /**
     * @Author: Koen Castermans
     * @Reviewer:
     */
    public function load_language(){
        $language = $this->CI->session->userdata('language');
        if($language == 'english'){
            $this->CI->lang->load('eng', 'english');
        } else {
            $this->CI->lang->load('ned', 'nederlands');
        }
    }
}