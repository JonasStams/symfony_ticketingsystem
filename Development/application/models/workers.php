<?php
/**
 * Created by PhpStorm.
 * User: Pieter
 * Date: 11/04/2016
 * Time: 13:37
 */
//dit moet nog uitgewerkt worden maar is gewoon om wat structuur te vinden in wat er gemaakt moet worden (een klasse ticket dus)-->kan pas uitgewerkt worden als
// er een normalisatie is gebeurt van de tabel
class Workers extends CI_Model
{
    private $table_name = 'workers';            // user accounts


    function __construct()
    {
        parent::__construct();
    }


    /**
     * @author Pieter Bollen
     * @edited Jonas Stams
     * @return	Object
     */
    function get_all_workers()
    {
        //   $query = $this->db->query("SELECT `users`.`id`, `users`.`username`, `users`.`email`, `user_roles`.`role`, `users`.`activated` FROM `user_roles` LEFT JOIN `users` ON `users`.`user_role` = `user_roles`.`id`;");

        $this->db->select('id','user_id','active');
        $this->db->from('workers');
        //$this->db->join('user_roles', 'user_roles.id = user_role');
        $query = $this->db->get();
        $workers = array();
        foreach ($query->result() as $row)
        {
            $workers[] = $row;
        }
        return $workers;
    }


    /**
     * @author Jonas Stams
     * @reviewer
     * @param	int
     * @return	int or NUll
     */
    function get_worker_id_by_user_id($userid)
    {
        //   $query = $this->db->query("SELECT `users`.`id`, `users`.`username`, `users`.`email`, `user_roles`.`role`, `users`.`activated` FROM `user_roles` LEFT JOIN `users` ON `users`.`user_role` = `user_roles`.`id`;");

        $this->db->select('id','user_id');
        $this->db->from('workers');
        $this->db->where('user_id=',$userid);
        //$this->db->join('user_roles', 'user_roles.id = user_role');
        $query = $this->db->get();

        if ($query->num_rows() == 1){
            $worker = $query->row();
            return $worker->id;
        }else{
            return NULL;
        }
    }

    function create_worker($data, $active = true){
        $data['active'] = $active;
        if ($this->db->insert($this->table_name, $data)) {
           return true;
        }
        return NULL;
    }

    function delete_worker($user_id)
    {
        $this->db->where('user_id', $user_id);
        $this->db->delete($this->table_name);
        if ($this->db->affected_rows() > 0) {
            return TRUE;
        }
        return FALSE;
    }

    /**
     * Jonas Stams
     * Pieter Bollen
     * @param $workerid
     * @return null
     */
    function get_user_id_by_worker_id($workerid)
    {
        //   $query = $this->db->query("SELECT `users`.`id`, `users`.`username`, `users`.`email`, `user_roles`.`role`, `users`.`activated` FROM `user_roles` LEFT JOIN `users` ON `users`.`user_role` = `user_roles`.`id`;");

        $this->db->select('id, user_id');
        $this->db->from('workers');
        $this->db->where('id=',$workerid);
        //$this->db->join('user_roles', 'user_roles.id = user_role');
        $query = $this->db->get();

        if ($query->num_rows() == 1){
            $worker = $query->row();
            return $worker->user_id;
        }else{
            return NULL;
        }
    }

}