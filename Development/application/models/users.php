<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Users
 *
 * This model represents user authentication data. It operates the following tables:
 * - user account data,
 * - user profiles
 *
 * @package	Tank_auth
 * @author	Ilya Konyukhov (http://konyukhov.com/soft/)
 */
class Users extends CI_Model
{
    private $table_name			= 'users';			// user accounts
    private $roles_table_name	= 'user_roles';			// user accounts
    private $profile_table_name	= 'user_profiles';	// user profiles

    function __construct()
    {
        parent::__construct();

        $ci =& get_instance();
        $this->table_name			= $ci->config->item('db_table_prefix', 'tank_auth').$this->table_name;
        $this->profile_table_name	= $ci->config->item('db_table_prefix', 'tank_auth').$this->profile_table_name;
        $this->roles_table_name	= $ci->config->item('db_table_prefix', 'tank_auth').$this->roles_table_name;
    }
    /**
     * Get all users
     *
     * @Author: Jonas Stams
     * @Reviewer:
     * @return	array
     */
    function get_all_users()
    {
        //   $query = $this->db->query("SELECT `users`.`id`, `users`.`username`, `users`.`email`, `user_roles`.`role`, `users`.`activated` FROM `user_roles` LEFT JOIN `users` ON `users`.`user_role` = `user_roles`.`id`;");

        $this->db->select('users.id, users.username, users.email, user_roles.role, users.banned');
        $this->db->from('users');
        $this->db->join('user_roles', 'user_roles.id = user_role');
        $query = $this->db->get();
        $users = array();
        foreach ($query->result() as $row)
        {
            $users[] = $row;
        }
        return $users;
    }

/**pieter Bollen
 * **/
    function get_all_users_by_status($status)
    {


        //   $query = $this->db->query("SELECT `users`.`id`, `users`.`username`, `users`.`email`, `user_roles`.`role`, `users`.`activated` FROM `user_roles` LEFT JOIN `users` ON `users`.`user_role` = `user_roles`.`id`;");

        $this->db->where('user_role=',$status);
        $query = $this->db->get($this->table_name);
        $users = array();
        foreach ($query->result() as $row)
        {
            $users[] = $row;
        }
        return $users;

    }
    /**
     * Get all users with pagination
     *
     * @Author: Jonas Stams
     * @Reviewer:
     * @return	array or false
     * @bron: http://www.sitepoint.com/pagination-with-codeigniter/
     */
    function get_all_users_pagination($limit, $start)
    {
        $this->db->limit($limit,$start);
        $this->db->select('users.id, users.username, users.email, users.user_role, user_roles.role, users.banned');
        $this->db->from('users');
        $this->db->join('user_roles', 'user_roles.id = user_role');
        $this->db->order_by("users.id", "asc");
        $query = $this->db->get();
        if($query->num_rows() > 0){
            $users = array();
            foreach ($query->result() as $row)
            {
                $users[] = $row;
            }
            return $users;
        }
        return false;
    }
    /**
     * Count all suers
     *
     * @Author: Jonas Stams
     * @Reviewer:
     * @return	int
     * @bron: http://www.sitepoint.com/pagination-with-codeigniter/
     */
    function user_count(){
        return $this->db->count_all("users");
    }

    /**
 * Get user record by Id
 *
 * @param	int
 * @param	bool
 * @return	object
 */
    function get_user_by_id($user_id, $activated)
    {
        $this->db->where('id', $user_id);
        $this->db->where('activated', $activated ? 1 : 0);

        $query = $this->db->get($this->table_name);
        if ($query->num_rows() == 1) return $query->row();
        return NULL;
    }



    /**
     * Checks if user can log in
     * @Bron: http://stackoverflow.com/questions/13518011/a-user-is-able-to-login-into-my-site-despite-not-having-existed-as-a-user-in-my
     * @param	int
     * @param	bool
     * @return	object
     */

    function get_users_by_userRole($limit, $start){

            $this->db->limit($limit,$start);
            $this->db->select('users.id, users.username, users.email, user_roles.role, users.banned');
            $this->db->from('users');
            $this->db->join('user_roles', 'user_roles.id = user_role');
            $this->db->where('user_roles','2');
            $this->db->order_by("users.id", "asc");
            $query = $this->db->get();
            if($query->num_rows() > 0){
                $users = array();
                foreach ($query->result() as $row)
                {
                    $users[] = $row;
                }
                return $users;
            }
            return false;
        }



    

    function can_log_in($email,$password)
    {

        $row = $this->db->get_where('users', array('email' => $email))->row();


        $this->db->where('email', $email);
        $query = $this->db->get('users');
        if ($query->num_rows() == 1)
        {
            $hash = $row->password;
            if ($this->bcrypt->check_password($password, $hash))
            {
                return true;
            }
            else
            {
                // Password does not match stored password.
            }
            return false;
        }

        else{

            return false;

        }
    }
    /**
     * Get user roles
     * @Author: Jonas Stams
     * @Reviewer:
     * @return	array
    @TODO: dynamische combobox maken in admins/index bij nieuwe user aanmaken.
    function get_user_roles() {
    $data = array();
    $query = $this->db->get($this->roles_table_name);
    if ($query->num_rows() > 0) {
    foreach ($query->result_array() as $row){
    $data[] = $row;
    }
    }
    $query->free_result();
    return $data;
    }
     */


    /**
     * Get user record by login (username or email)
     *
     * @param	string
     * @return	object
     */
    function get_user_by_login($login)
    {
        $this->db->where('LOWER(username)=', strtolower($login));
        $this->db->or_where('LOWER(email)=', strtolower($login));

        $query = $this->db->get($this->table_name);
        if ($query->num_rows() == 1) return $query->row();
        return NULL;
    }

    /**
     * Get user record by username
     *
     * @param	string
     * @return	object
     */
    function get_user_by_username($username)
    {
        $this->db->where('LOWER(username)=', strtolower($username));

        $query = $this->db->get($this->table_name);
        if ($query->num_rows() == 1) return $query->row();
        return NULL;
    }

    /**
     * Get user record by email
     *
     * @param	string
     * @return	object
     */
    function get_user_by_email($email)
    {
        $this->db->where('LOWER(email)=', strtolower($email));

        $query = $this->db->get($this->table_name);
        if ($query->num_rows() == 1) return $query->row();
        return NULL;
    }

    /**
     * Check if username available for registering
     *
     * @param	string
     * @return	bool
     */
    function is_username_available($username)
    {
        $this->db->select('1', FALSE);
        $this->db->where('LOWER(username)=', strtolower($username));

        $query = $this->db->get($this->table_name);
        return $query->num_rows() == 0;
    }

    /**
     * Check if email available for registering
     *
     * @param	string
     * @return	bool
     */
    function is_email_available($email)
    {
        $this->db->select('1', FALSE);
        $this->db->where('LOWER(email)=', strtolower($email));
        $this->db->or_where('LOWER(new_email)=', strtolower($email));

        $query = $this->db->get($this->table_name);
        return $query->num_rows() == 0;
    }

    /**
     * Create new user record
     *
     * @param	array
     * @param	bool
     * @return	array
     */
    function create_user($data, $activated = TRUE)
    {
        $data['created'] = date('Y-m-d H:i:s');
        $data['activated'] = $activated ? 1 : 0;


        if ($this->db->insert($this->table_name, $data)) {
            $user_id = $this->db->insert_id();
            if($data['user_role'] == 4){
                $worker = array();
                $worker['user_id'] = $user_id;
                $this->load->model('workers');
                $this->workers->create_worker($worker);
        }
          //  if ($activated)	$this->create_profile($user_id);
            return array('user_id' => $user_id);
        }
        return NULL;
    }

    /**
     * Activate user if activation key is valid.
     * Can be called for not activated users only.
     *
     * @param	int
     * @param	string
     * @param	bool
     * @return	bool
     */
    function activate_user($user_id, $activation_key, $activate_by_email)
    {
        $this->db->select('1', FALSE);
        $this->db->where('id', $user_id);
        if ($activate_by_email) {
            $this->db->where('new_email_key', $activation_key);
        } else {
            $this->db->where('new_password_key', $activation_key);
        }
        $this->db->where('activated', 0);
        $query = $this->db->get($this->table_name);

        if ($query->num_rows() == 1) {

            $this->db->set('activated', 1);
            $this->db->set('new_email_key', NULL);
            $this->db->where('id', $user_id);
            $this->db->update($this->table_name);

            $this->create_profile($user_id);
            return TRUE;
        }
        return FALSE;
    }

    /**
     * Purge table of non-activated users
     *
     * @param	int
     * @return	void
     */
    function purge_na($expire_period = 172800)
    {
        $this->db->where('activated', 0);
        $this->db->where('UNIX_TIMESTAMP(created) <', time() - $expire_period);
        $this->db->delete($this->table_name);
    }

    /**
     * Delete user record
     *
     * @param	int
     * @return	bool
     */
    function delete_user($user_id)
    {
        $this->db->where('id', $user_id);
        $this->db->delete($this->table_name);
        if ($this->db->affected_rows() > 0) {
            $this->delete_profile($user_id);
            return TRUE;
        }
        return FALSE;
    }


    /**
     * Set new password key for user.
     * This key can be used for authentication when resetting user's password.
     *
     * @param	int
     * @param	string
     * @return	bool
     */
    function set_password_key($user_id, $new_pass_key)
    {
        $this->db->set('new_password_key', $new_pass_key);
        $this->db->set('new_password_requested', date('Y-m-d H:i:s'));
        $this->db->where('id', $user_id);

        $this->db->update($this->table_name);
        return $this->db->affected_rows() > 0;
    }

    /**
     * Check if given password key is valid and user is authenticated.
     *
     * @param	int
     * @param	string
     * @param	int
     * @return	void
     */
    function can_reset_password($user_id, $new_pass_key, $expire_period = 900)
    {
        $this->db->select('1', FALSE);
        $this->db->where('id', $user_id);
        $this->db->where('new_password_key', $new_pass_key);
        $this->db->where('UNIX_TIMESTAMP(new_password_requested) >', time() - $expire_period);

        $query = $this->db->get($this->table_name);
        return $query->num_rows() == 1;
    }

    /**
     * Change user password if password key is valid and user is authenticated.
     *
     * @param	int
     * @param	string
     * @param	string
     * @param	int
     * @return	bool
     */
    function reset_password($user_id, $new_pass, $new_pass_key, $expire_period = 900)
    {
        $this->db->set('password', $new_pass);
        $this->db->set('new_password_key', NULL);
        $this->db->set('new_password_requested', NULL);
        $this->db->where('id', $user_id);
        $this->db->where('new_password_key', $new_pass_key);
        $this->db->where('UNIX_TIMESTAMP(new_password_requested) >=', time() - $expire_period);

        $this->db->update($this->table_name);
        return $this->db->affected_rows() > 0;
    }

    /**
     * Change user password
     *
     * @param	int
     * @param	string
     * @return	bool
     */
    function change_password($user_id, $new_pass)
    {
        $this->db->set('password', $new_pass);
        $this->db->where('id', $user_id);

        $this->db->update($this->table_name);
        return $this->db->affected_rows() > 0;
    }

    /**
     * Set new email for user (may be activated or not).
     * The new email cannot be used for login or notification before it is activated.
     *
     * @param	int
     * @param	string
     * @param	string
     * @param	bool
     * @return	bool
     */
    function set_new_email($user_id, $new_email, $new_email_key, $activated)
    {
        $this->db->set($activated ? 'new_email' : 'email', $new_email);
        $this->db->set('new_email_key', $new_email_key);
        $this->db->where('id', $user_id);
        $this->db->where('activated', $activated ? 1 : 0);

        $this->db->update($this->table_name);
        return $this->db->affected_rows() > 0;
    }

    /**
     * Activate new email (replace old email with new one) if activation key is valid.
     *
     * @param	int
     * @param	string
     * @return	bool
     */
    function activate_new_email($user_id, $new_email_key)
    {
        $this->db->set('email', 'new_email', FALSE);
        $this->db->set('new_email', NULL);
        $this->db->set('new_email_key', NULL);
        $this->db->where('id', $user_id);
        $this->db->where('new_email_key', $new_email_key);

        $this->db->update($this->table_name);
        return $this->db->affected_rows() > 0;
    }

    /**
     * Update user login info, such as IP-address or login time, and
     * clear previously generated (but not activated) passwords.
     *
     * @param	int
     * @param	bool
     * @param	bool
     * @return	void
     */
    function update_login_info($user_id, $record_ip, $record_time)
    {
        $this->db->set('new_password_key', NULL);
        $this->db->set('new_password_requested', NULL);

        if ($record_ip)		$this->db->set('last_ip', $this->input->ip_address());
        if ($record_time)	$this->db->set('last_login', date('Y-m-d H:i:s'));

        $this->db->where('id', $user_id);
        $this->db->update($this->table_name);
    }

    /**
     * Check if username available for registerin
     * @Author: Jonas Stams
     * @Reviewer:
     */
    function check_ban_status($user_id)
    {
        $this->db->where('id', $user_id);
        $query = $this->db->get($this->table_name);
        $user = $query->row();
        return $user->banned;
    }

    /**
     * Ban user
     *
     * @param	int
     * @param	string
     * @return	void
     */
    function ban_user($user_id, $reason = NULL)
    {
        $this->db->where('id', $user_id);
        $this->db->update($this->table_name, array(
            'banned'		=> 1,
            'ban_reason'	=> $reason
        ));
    }

    /**
     * Unban user
     *
     * @param	int
     * @return	void
     */
    function unban_user($user_id)
    {
        $this->db->where('id', $user_id);
        $this->db->update($this->table_name, array(
            'banned'		=> 0,
            'ban_reason'	=> NULL,
        ));
    }

    /**
     * Create an empty profile for a new user
     *
     * @param	int
     * @return	bool
     */
    private function create_profile($user_id)
    {
        $this->db->set('user_id', $user_id);
        return $this->db->insert($this->profile_table_name);
    }

    /**
     * Delete user profile
     *
     * @param	int
     * @return	void
     */
    private function delete_profile($user_id)
    {
        $this->db->where('user_id', $user_id);
        $this->db->delete($this->profile_table_name);
    }

    //Search for results
    //http://stackoverflow.com/questions/21310121/codeigniter-pagination-showing-all-query-results-in-one-page
    public function search($id, $table, $pageNumber) {

        //Get the number of pages
        $numOfPage = $this->findResults($id, $table, RETURN_NUM_OF_PAGES, $pageNumber);

        if ($numOfPage < 1) {
            return false; //If there are no search results return false
        } else {
            $row = array();
            $res = $this->findResults($id, $table, RETURN_RESULTS, $pageNumber);
            for ($j = 0; $j < $res->num_rows(); $j++) {
                $row[$j] = $res->row($j);
            }
            return $row; //Return the results
        }
    }

    // Find the results from DB and return number of pages/ results
    function find_results($id, $table, $returnType, $pageNumber) {

        $this->db->where('id', $id);
        $this->db->order_by('reputation', 'desc'); //Order the users by reputation
        $itemsPerPage = 5;

        $startingItem = ($pageNumber - 1) * $itemsPerPage;

        if ($returnType == RETURN_RESULTS) {
            $res = $this->db->get($table, $itemsPerPage, $startingItem);
            return $res; //Return the results
        } else { //Return the number of pages
            $res = $this->db->get($table);
            $count = $res->num_rows(); //Get the total number of results

            $numofPages = (int) ($count / $itemsPerPage);
            if ($count % $itemsPerPage != 0)
                $numofPages = $numofPages + 1; //If the number of items < $itemsPerPage there should be 1 page
            return $numofPages; //Return the number of pages
        }
    }
}

/* End of file users.php */
/* Location: ./application/models/auth/users.php */