<?php

/**
 * Created by PhpStorm.
 * User: Jonas
 * Date: 27/05/2016
 * Time: 18:41
 */

class ticket_message extends CI_Model
{
    var $ticket_id   = '';
    var $user_id = '';
    var $message    = '';

    /**
     * Ticket_message constructor.
     */
    public function __construct()
    {
    }

    /**
     * @return string
     */
    public function get_ticket_id()
    {
        return $this->ticket_id;
    }

    /**
     * @param string $ticket_id
     */
    public function set_ticket_id($ticket_id)
    {
        $this->ticket_id = $ticket_id;
    }

    /**
     * @return string
     */
    public function get_user_id()
    {
        return $this->user_id;
    }

    /**
     * @param string $user_id
     */
    public function set_user_id($user_id)
    {
        $this->user_id = $user_id;
    }

    /**
     * @return string
     */
    public function get_message()
    {
        return $this->message;
    }

    /**
     * @param string $message
     */
    public function set_message($message)
    {
        $this->message = $message;
    }


}