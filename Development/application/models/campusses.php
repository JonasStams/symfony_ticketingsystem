<?php
/**
 * Created by PhpStorm.
 * User: Sander Jordens
 * Date: 7/05/2016
 * Time: 10:04
 */
class Campusses extends CI_Model
{

    private $table_name = 'campusses';


    function __construct()
    {
        parent::__construct();
    }

    /**
     * User: Sander Jordens
     * @reviewer
     * @param $id
     * @return	Object or false
     */

    function get_description_by_id($id)
    {
        $this->db->select('id,description');
        $this->db->from($this->table_name);
        $this->db->where('id=',$id);
        $query = $this->db->get();
        if ($query->num_rows() == 1){
            $row = $query->row();
            return $row->description;
        }
        return false;
    }




}