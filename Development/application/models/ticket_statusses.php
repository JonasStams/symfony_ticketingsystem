<?php

/**
 * Created by PhpStorm.
 * User: Jonas Stams
 * Date: 5/05/2016
 * Time: 20:59
 */
class Ticket_statusses extends CI_Model{
    private $table_name = 'ticket_status';

    function __construct()
    {
        parent::__construct();

    }

    /**
     * User: Jonas Stams
     * @reviewer
     * @param $id
     * @return	array or false
     */
    function get_description_by_id($id)
    {
        $this->db->select('id,description');
        $this->db->from($this->table_name);
        $this->db->where('id=',$id);
        $query = $this->db->get();
        if ($query->num_rows() == 1){
           $row = $query->row();
            return $row->description;
        }
        return false;
    }



}
