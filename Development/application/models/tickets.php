<?php
/**
 * Created by PhpStorm.
 * User: Pieter Bollen
 * Date: 11/04/2016
 * Time: 13:37
 */
//dit moet nog uitgewerkt worden maar is gewoon om wat structuur te vinden in wat er gemaakt moet worden (een klasse ticket dus)-->kan pas uitgewerkt worden als
// er een normalisatie is gebeurt van de tabel
class Tickets extends CI_Model
{
    private $table_name = 'tickets';            // user accounts
    private $status_table_name = 'status';            // user accounts
    private $ticket_table_name = 'user_profiles';    // user profiles

    function __construct()
    {
        parent::__construct();

    }


    /**
     * @author Pieter Bollen
     * @reviewer
     * @return	array
     */
    function get_all_tickets()
    {

        $this->db->select('id, assigned_by, assigned_to, created_by, campus, status, place,
                                       subject, description, priority,created_on');
        $this->db->from('tickets');
        $query = $this->db->get();
        $tickets = array();
        foreach ($query->result() as $row) {
            $tickets[] = $row;
        }
        return $tickets;
    }

    /**
     * @author  Pieter Bollen
     * @reviewer
     * @param   int
     * @return  array
     */
    function get_tickets_by_creator($id)
    {
        $this->db->select('id, assigned_by, assigned_to, created_by, campus, status, place,
                                       subject, description, priority, created_on');
        $this->db->from('tickets');
        $this->db->where('created_by=', $id);
        $query = $this->db->get();
        $tickets = array();
        foreach ($query->result() as $row) {
            $tickets[] = $row;
        }
        return $tickets;

    }

    /**
     * @author
     * @reviewer
     * @param   int
     * @return  array
     */
    function get_all_tickets_by_status($status)
    {
        $this->db->select('id, assigned_to, created_by, campus, status, place,
                                       subject, description, priority, created_on');
        $this->db->from('tickets');
        $this->db->where('status=', $status);
        $query = $this->db->get();
        $tickets = array();
        foreach ($query->result() as $row) {
            $tickets[] = $row;
        }
        return $tickets;
    }

    /**
     * @author  Pieter Bollen
     * @reviewer
     * @param   int
     * @return  array
     */
    function get_all_tickets_by_creator_pagination($id, $limit, $start)
    {
        $this->db->limit($limit, $start);
        $this->db->select('id, assigned_to, created_by, campus, status, place,
                                       subject, description, priority, created_on');
        $this->db->from('tickets');
        $this->db->where('created_by=', $id);
        $query = $this->db->get();
        $tickets = array();
        foreach ($query->result() as $row) {
            $tickets[] = $row;
        }
        return $tickets;

    }
    /**
     * @author  Pieter Bollen
     * @reviewer
     * @param   int
     * @return  array or NULL
     */
    function get_ticket_info_by_id($id)
    {

        $this->db->where('id', $id);
        $query = $this->db->get($this->table_name);
        if ($query->num_rows() == 1) return $query->row();
        return NULL;
    }



    /**
     * @author  Pieter Bollen
     * @reviewer
     * @param   int
     * @return  array or false
     */
    function get_all_tickets_pagination($limit, $start)
    {
        $this->db->limit($limit, $start);
        $this->db->select('id, assigned_by, assigned_to, created_by, campus, status, place,
                                       subject, description, priority, created_on');
        $this->db->from('tickets');
        $this->db->order_by("id", "asc");
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $tickets = array();
            foreach ($query->result() as $row) {
                $tickets[] = $row;
            }
            return $tickets;
        }
        return false;
    }
    /**
     * @author  Jonas Stams
     * @reviewer
     * @reviewer
     * @param   int
     * @return  array or false
     */
    function get_all_tickets_by_worker_id($id,$limit, $start)
    {
        $this->db->limit($limit, $start);
        $this->db->select('id, assigned_to, created_by, campus, status, place,
                                       subject, description, priority, created_on');
        $this->db->from('tickets');
        $this->db->where('assigned_to=',$id);
        $this->db->order_by("status", "asc");
        $this->db->order_by("campus", "asc");
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $tickets = array();
            foreach ($query->result() as $row) {
                $tickets[] = $row;
            }
            return $tickets;
        }
        return false;
    }
    /**
     * @author Jonas Stams
     * @reviewer
     * @reviewer
     * @param   int
     * @return  int
     */
    function count_all_tickets_by_status_and_worker_id($status,$id)
    {
        $this->db->from('tickets');
        $this->db->where('status=',$status);
        $this->db->where('assigned_to=',$id);
        $query = $this->db->get();
        return $query->num_rows();
    }


    /**
     * @author Pieter Bollen
     * @reviewer
     * @param   Object
     * @return  true or false
     */
    function create_ticket($ticket)
    {
        $this->db->insert($this->table_name, $ticket);

        if ($this->db->affected_rows() > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    /**
     * @author Jonas Stams
     * @reviewer
     * @param   int
     * @return  true or false
     */
    function change_status($id, $status)
    {
        $this->load->model('ticket_messages');
        $this->load->model('ticket_message');
        $this->load->model('ticket_statusses');

        $this->db->set('status', $status);
        $this->db->where('id=', $id);
        $this->db->update($this->table_name);
        if($this->db->affected_rows() > 0){
            $message = $this->ticket_message;
            $message->ticket_id = $id;
            $status_desc = $this->ticket_statusses->get_description_by_id($status);
            $message->user_id = $this->session->userdata('id');
            $message->message = 'Status bijgewerkt naar ' . $status_desc;
            $this->ticket_messages->create_message($message);
            return TRUE;
        }else{
            return FALSE;
        }


    }

    /**
     * @author Pieter Bollen
     * @reviewer
     * @param int
     * @return  true or false
     */
    function delete_ticket($ticket_id)
    {
        $this->db->where('id', $ticket_id);
        $this->db->delete($this->table_name);
        if ($this->db->affected_rows() > 0) {
            return TRUE;
        }
        return FALSE;
    }

    /**
     * @author Pieter Bollen
     * @reviewer
     * @return  int
     */
    function ticket_count()
    {
        return $this->db->count_all("tickets");
    }

    /**
     * @author Pieter Bollen
     * @reviewer
     * @param
     * @return
     */
    function update_ticket($ticket_id, $new_campus, $new_place,$new_subject,$new_description)
    {
        $this->db->set('campus',$new_campus) ;
        $this->db->set('place',$new_place);
        $this->db->set('subject',$new_subject);
        $this->db->set('description',$new_description);
        $this->db->where('id',$ticket_id);
        $this->db->update($this->table_name);
        return $this->db->affected_rows() > 0;

    }

    /**
     * @author Pieter Bollen
     * @reviewer
     * @param
     * @return
     */
    function update_ticket_status($ticket_id,$status){
        $this->db->set('status',$status);
        $this->db->where('id',$ticket_id);
        $this->db->update($this->table_name);
        return $this->db->affected_rows()>0;
    }

    /**
     * @author Pieter Bollen
     * @reviewer
     * @param
     * @return
     */
    function update_ticket_assigned_to($ticket_id,$user_id){
        $this->load->model('workers');
        $worker_id = $this->workers->get_worker_id_by_user_id($user_id);
        $this->db->set('assigned_to',$worker_id);
        $this->db->where('id',$ticket_id);
        $this->db->update($this->table_name);
        return $this->db->affected_rows()>0;
    }
/**
* @author Jonas Stams
* @reviewer Pieter Bollen
* @param   int
* @return  array
*/
    function get_all_tickets_without_worker($limit, $start)
    {

        $this->db->limit($limit,$start);
        $this->db->select('id, assigned_to, created_by, campus, status, place,
                                       subject, description, priority, created_on');
        $this->db->from('tickets');
        $this->db->where('assigned_to=', null);
        $query = $this->db->get();
        $tickets = array();
        foreach ($query->result() as $row) {
            $tickets[] = $row;
        }
        return $tickets;
    }


    function count_all_tickets_without_worker(){
        $this->db->select('id, assigned_to, created_by, campus, status, place,
                                       subject, description, priority, created_on');
        $this->db->from('tickets');
        $this->db->where('assigned_to=', null);
        $query = $this->db->get();
        $tickets = array();
        foreach ($query->result() as $row) {
            $tickets[] = $row;
        }
        return count($tickets);

    }




}