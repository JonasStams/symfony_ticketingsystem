<?php

/**
 * Created by PhpStorm.
 * User: Jonas
 * Date: 5/05/2016
 * Time: 14:41
 */
class ticket_messages extends CI_Model
{
    private $table_name = 'ticket_messages';

    function __construct()
    {
        parent::__construct();


    }

    /**
     * @author Jonas Stams
     * @reviewer
     * @param	int
     * @return	True or False
     */
    function get_all_messages_per_ticket_id($id)
    {

        $this->db->select('id,ticket_id,user_id,message,created');
        $this->db->from($this->table_name);
        $this->db->where('ticket_id=',$id);
        $query = $this->db->get();
        if($query->num_rows() > 0){
            $messages = array();
            foreach ($query->result() as $row)
            {

                $messages[] = $row;
            }
            return $messages;
        }
        return false;
    }

    function create_message($message)
    {
        if ($this->db->insert($this->table_name, $message)) {
            return TRUE;
        }
        return FALSE;
    }
}