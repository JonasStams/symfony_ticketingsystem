<?php
/**
 * Created by PhpStorm.
 * @Author: Jonas Stams
 * @Reviewer: Koen Castermans
 * Date: 23/03/2016
 * Time: 15:57
 */
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title><?php echo $this->lang->line('view_members_title'); ?></title>

    <!-- Jquery -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
    <!-- Fonts -->
    <link href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700|Lato:400,100,300,700,900'
          rel='stylesheet' type='text/css'>
    <!-- Responsiveness -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css"
          integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css"
          integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"
            integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS"
            crossorigin="anonymous"></script>
    <!-- CSS paginas -->
    <?= link_tag('assets/css/main.css') ?>
    <?= link_tag('assets/css/animate.css') ?>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
</head>
<body>


<!--
* @Author: Pieter Bollen
* @Reviewer:
* Date: 00/00/2016
* Time: 00:00
-->

<div class="container-members">
    <div class="row">
        <div class="col-md-4">
        </div>
        <div class="col-md-4">
            <?php if ($this->session->userdata('user_role') == 1) {
                echo "<p class='navbar-text navbar-right'><a href='" . base_url() . "admins'>". $this->lang->line('view_members_admin_panel') . "</a></p>";
            }; ?>
            <?php if ($this->session->userdata('user_role') == 3) {
                echo "<p class='navbar-text navbar-right'><a href='" . base_url() . "dispatchers'>" . $this->lang->line('view_members_dispatcher_panel') . "</a></p>";
            }; ?>
            <?php if ($this->session->userdata('user_role') == 4) {
                echo "<p class='navbar-text navbar-right'><a href='" . base_url() . "workers'>" . $this->lang->line('view_members_worker_panel') . "</a></p>";
            }; ?>
            <?php if ($this->session->userdata('user_role') == 2) {
                echo "<p class='navbar-text navbar-right'><a href='" . base_url() . "members'>" . $this->lang->line('user_panel') . "</a></p>";
            }; ?>

        </div>
        <div class="col-md-4">
            <p class="navbar-text navbar-right">
                <?php echo $this->lang->line('signed_in'); ?>
                <a href="#"><?php print_r($this->session->userdata('username')) ?></a> |
                <a href='<?php echo base_url() . "main/logout" ?>'> <?php echo $this->lang->line('logout'); ?></a>
            </p>

        </div>

        <div class="row">
            <div class="col-md-6">

                <h1><img src='<?php echo base_url() . "assets/img/pxl.png" ?>' alt="Logo pxl" class="logo-sm"/> | <?php echo $this->lang->line('system_name'); ?></h1>
            </div>

        </div>


        <div id="menu-tickets">

            <div class="row">
                <div class="col-md-6">
                    <h3><?php echo  $this->lang->line('view_members_h3_tickets'); ?></h3>
                    <p><?php echo  $this->lang->line('view_members_info_tickets'); ?></p>
                </div>
                <div class="col-md-6">
                    <form class="navbar-form navbar-right" role="search">
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="<?php echo $this->lang->line('view_members_search_placeholder') ?>" >
                        </div>
                        <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                    </form>
                </div>

            </div>
            <div class="row">
                <div class="col-md-5">
                    <h4><?php echo $this->lang->line('view_members_h4_create_ticket'); ?></h4>
                    <p style="height:35px"><?php echo $this->lang->line('view_members_info_create_ticket'); ?></p>
                    <button class="btn btn-success" style="margin-bottom:25px" id="create_ticket_btn" type="button">
                        <i class="fa fa-plus"></i> <?php echo $this->lang->line('view_members_button_add_ticket'); ?>
                    </button>
                </div>
                <div class="col-md-7">
                    <h4><?php echo $this->lang->line('view_members_h4_edit_tickets'); ?></h4>
                    <p style="height:35px"><?php echo $this->lang->line('view_members_info_edit_tickets'); ?></p>

                    <?php echo form_open('members/ticket_table_handler'); ?>
                    <button class="btn btn-danger" style="margin-bottom:25px" name="sbm"
                            onclick="return confirm('<?php echo $this->lang->line('view_members_button_mention_delete'); ?>');" value="delete_tickets"
                            type="submit">
                        <i class="fa fa-trash"></i> <?php echo $this->lang->line('view_members_button_delete_ticket'); ?>
                    </button>
                    <button class="btn btn-info" style="margin-bottom:25px" name="sbm" id="edit_ticket_btn"
                            value="edit_ticket" type="submit">
                        <i class="fa fa-pencil-square-o"></i> <?php echo $this->lang->line('view_members_button_modify_ticket'); ?>
                    </button>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <?php
                        $message = $this->session->flashdata('message');
                        $warning = $this->session->flashdata('warning');
                        $error = $this->session->flashdata('error');
                        if (isset($message)) {
                            echo "<div class='alert alert-success' role='alert'>" . $message . "</div>";
                        }
                        if (isset($warning)) {
                            echo "<div class='alert alert-warning' role='alert'>" . $warning . "</div>";
                        }
                        if (isset($error)) {
                            echo "<div class='alert alert-danger' role='alert'>" . $error . "</div>";
                        }
                        if (isset($all_tickets)) {
                            $chk_settings = array(
                                'id'          => 'select_all',
                                'checked'     => FALSE
                            );
                            $status='';
                            $tickets[] = array(form_checkbox($chk_settings),'#', $this->lang->line('ticket_table_campus'), $this->lang->line('ticket_table_place'),$this->lang->line('ticket_table_description'),$this->lang->line('ticket_table_subject'),$this->lang->line('ticket_table_status'),$this->lang->line('ticket_table_created')); //kolom namen meegeven voor de table
                            /**
                             * bijgevoegd door: Jonas Stams
                             * ------------------------------
                             */
                            foreach($all_tickets as $ticket){
                                switch(strtolower($ticket->campus)){
                                    case "1":
                                        $campus = "<span class='label label-primary'>Diepenbeek</span>";
                                        break;
                                    case "2":
                                        $campus = "<span class='label label-info'>Elfde Linie</span>";
                                        break;
                                    case "3":
                                        $campus = "<span class='label label-warning'>Guffenslaan</span>";
                                        break;
                                    case "4":
                                        $campus = "<span class='label label-default'>Quartier Canal</span>";
                                        break;
                                    case "5":
                                        $campus ="<span class='label label-default'>Vildersstraat</span>";
                                }
                                /**
                                 * ----------------------------
                                 */
                                switch(strtolower($ticket->status)){
                                    case "1":
                                        $status = "<span class='label label-primary'>Awaiting Operative</span>";
                                        break;
                                    case "2":
                                        $status = "<span class='label label-info'>In Progress</span>";
                                        break;
                                    case "3":
                                        $status = "<span class='label label-warning'>Solved</span>";
                                        break;
                                    case "4":
                                        $status = "<span class='label label-default'>On hold</span>";
                                        break;
                                    case "5":
                                        $status="<span class='label label-default'>Closed</span>";
                                }

                                $chk_settings = array(
                                    'name'        => 'select['.$ticket->id.']',
                                    'id'          => 'select['.$ticket->id.']',
                                    'class'       => 'checkbox',
                                    'value'       => $ticket->id,
                                    'checked'     => FALSE
                                );
                                //Fout aangepast door Koen Castermans -> campusnaam ipv campuscijfer
                                $tickets[] = array(form_checkbox($chk_settings),$ticket->id, ucfirst($campus),
                                    ucfirst($ticket->place), ucfirst($ticket->description),ucfirst($ticket->subject), $status, $ticket->created_on); //tickets in array zetten
                            }
                            $this->load->library('table');                //inladen van library voor tables
                            $tmpl = array(
                                'table_open' => '<table id="ticket_table" class="table table-striped memb smartph extra">',
                                'heading_row_start' => "<tr style='height: 45px;background-color: #C7E9A4;'>"
                            );
                            $this->table->set_template($tmpl);
                            echo "<div class='table-responsive'>";
                            echo $this->table->generate($tickets);
                            echo form_close();
                            echo '<p>' . $links . '</p>';
                            echo "</div>";
                        }; ?>

                    </div>
                </div>

                <!--
                * @Author: Pieter Bollen
                * @Reviewer:
                * Date: 00/00/2016
                * Time: 00:00
                -->

                <!--ticket aanmaken model-->

                <div class="modal fade" id="myTicketModel" role="dialog">
                    <div class="modal-dialog">

                        <div class="modal-content" style="text-align: center;">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 style="font-size:1.6em;"><?php echo $this->lang->line('view_members_modal_h4_create_ticket'); ?></h4>
                            </div>
                            <div class="modal-body" style="padding:40px 50px; background-color:white">

                                <?php echo form_open('members/create_ticket');

                                $userid = array(
                                    'name' => 'userid',
                                    'id' => 'userid',
                                    'maxlength' => '100',
                                    'size' => '4',
                                    'style' => 'width:50%',
                                );

                                $place = array(
                                    'name' => 'place',
                                    'id' => 'place',
                                    'type' => 'place',
                                    'maxlength' => '100',
                                    'size' => '50',
                                    'style' => 'width:50%',
                                );
                                $description = array(
                                    'name' => 'description',
                                    'id' => 'description',
                                    'maxlength' => '300',
                                    'size' => '50',
                                    'style' => 'width:50%',
                                );
                                $subject = array(
                                    'name' => 'subject',
                                    'id' => 'subject',
                                    'maxlength' => '100',
                                    'size' => '150',
                                    'style' => 'width:50%',
                                );


                                if (isset($view)) {
                                    $value = $view;
                                } else {
                                    $value = 0;
                                }
                                $comeback = array(
                                    'name' => 'comeback',
                                    'id' => 'comeback',
                                    'value' => $value,
                                    'type' => 'hidden',
                                    'maxlength' => '1',
                                    'size' => '1',

                                );
                                echo form_input($comeback);

                                // echo validation_errors();
                                echo form_label($this->lang->line('view_members_modal_label_campus'));
                                echo "<br/>";
                                ?>
                                <select name='campus' id='campus' style="width:50%">
                                    <option value='1'>Diepenbeek</option>
                                    <option value='2' selected='selected'>Elfde Linie</option>
                                    <option value='3'>Guffenslaan</option>
                                    <option value='4'>Quartier Canal</option>
                                    <option value='5'>Vildersstraat</option>
                                </select>
                                <?php
                                echo "<br/>";
                                echo form_label($this->lang->line('view_members_modal_label_place'));
                                echo "<br/>";
                                echo form_input($place);
                                echo "<br/>";
                                echo form_label($this->lang->line('view_members_modal_label_subject'));
                                echo "<br/>";
                                echo form_input($subject);
                                echo "<br/>";
                                echo form_label($this->lang->line('view_members_modal_label_description'));
                                echo "<br/>";
                                echo form_textarea($description);

                                echo "<hr><br/>";


                                echo form_submit('create_submit', $this->lang->line('view_members_modal_create_submit_save'));
                                echo form_close(); ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal fade" id="edit-ticketmodal" role="dialog">
                    <div class="modal-dialog">

                        <div class="modal-content" style="text-align: center;">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 style="font-size:1.6em;"><?php echo $this->lang->line('view_members_modal_modify_h4'); ?></h4>
                            </div>
                            <div class="modal-body" style="padding:40px 50px; background-color:white">
                                <h5><?php echo $this->lang->line('view_members_modal_modify_h5'); ?></h5>
                                <hr>
                                <?php echo form_open('members/update_ticket');
                                if (isset($ticketdata)) {
                                    $campusvalue = $ticketdata['campus'];
                                    $placevalue = $ticketdata['place'];
                                    $subjectvalue = $ticketdata['subject'];
                                    $descriptionvalue = $ticketdata['description'];
                                } else {
                                    $campusvalue = '';
                                    $placevalue = '';
                                    $subjectvalue = '';
                                    $descriptionvalue = '';
                                }

                                $place = array(
                                    'name' => 'place',
                                    'id' => 'place',
                                    'type' => 'place',
                                    'value' => $placevalue,
                                    'maxlength' => '100',
                                    'size' => '50',
                                    'style' => 'width:50%',
                                );
                                $description = array(
                                    'name' => 'description',
                                    'id' => 'description',
                                    'value' => $descriptionvalue,
                                    'maxlength' => '300',
                                    'size' => '50',
                                    'style' => 'width:50%',
                                );
                                $subject = array(
                                    'name' => 'subject',
                                    'id' => 'subject',
                                    'maxlength' => '100',
                                    'value' => $subjectvalue,
                                    'size' => '50',
                                    'style' => 'width:50%',
                                );
                                if (!empty($this->session->flashdata('modalopen'))) {
                                    $value = $this->session->flashdata('modalopen');
                                } else {
                                    $value = 0;
                                }
                                $comeback = array(
                                    'name' => 'comeback',
                                    'id' => 'comeback',
                                    'value' => $value,
                                    'type' => 'hidden',
                                    'maxlength' => '1',
                                    'size' => '1',

                                );
                                echo form_input($comeback);
                                echo form_hidden('user_id', $this->session->userdata('id'));
                                echo form_hidden('ticket_id', $ticket_id);
                                echo form_label($this->lang->line('view_members_modal_label_campus'));
                                echo "<br />"
                                ?>
                                <!--Toegevoegd door Koen Castermans-->
                                <select name='campus' id='campus' style="width:50%">
                                    <?php
                                        if($campusvalue == 1){
                                            echo "<option value='1' selected='selected'>Diepenbeek</option>";
                                        } else {
                                            echo "<option value='1'>Diepenbeek</option>";
                                        }

                                        if($campusvalue == 2){
                                            echo "<option value='2' selected='selected'>Elfde Linie</option>";
                                        } else {
                                            echo "<option value='2'>Elfde Linie</option>";
                                        }

                                        if($campusvalue == 3){
                                            echo "<option value='3' selected='selected'>Guffenslaan</option>";
                                        } else {
                                            echo "<option value='3'>Guffenslaan</option>";
                                        }

                                        if($campusvalue == 4){
                                            echo "<option value='4' selected='selected'>Quartier Canal</option>";
                                        } else {
                                            echo "<option value='4'>Quartier Canal</option>";
                                        }

                                        if($campusvalue == 5){
                                            echo "<option value='5' selected='selected'>Vildersstraat</option>";
                                        } else {
                                            echo "<option value='5'>Vildersstraat</option>";
                                        }
                                    ?>
                                </select>
                                <!-- EINDE Toegevoegd door Koen Castermans-->
                                <?php
                                echo "<br/>";
                                echo form_label($this->lang->line('view_members_modal_label_place'));
                                echo "<br/>";
                                echo form_input($place);
                                echo "<br/>";
                                echo form_label($this->lang->line('view_members_modal_label_description'));
                                echo "<br/>";
                                echo form_input($description);
                                echo "<br/>";
                                echo form_label($this->lang->line('view_members_modal_label_subject'));
                                echo "<br/>";
                                echo form_input($subject);
                                echo "<br/>";
                                if (isset($message)) {
                                    echo "<div class='alert alert-succes' role='alter'>" . $message . "</p>";
                                }
                                if (isset($warning)) {
                                    echo "<div class='alert alert-warning' role='alert'>" . $warning . "</div>";
                                }
                                if (isset($error)) {
                                    echo "<div class='alert alert-danger' role='alert'>" . $error . "</div>";
                                }
                                echo "<br/>";
                                echo form_submit('edit_usr', $this->lang->line('view_members_modal_modify_submit_save'));
                                echo form_close(); ?>

                            </div>
                        </div>
                    </div>
                </div>
                <!-- einde model -->
            </div>
        </div>
    </div>
</div>

</body>
<script>
    $(document).ready(function () {
        $("#create_ticket_btn").click(function () {
            $("#myTicketModel").modal();
        });
        $("#create_ticket_menu").click(function () {
            $("#myTicketModel").modal();
        });
        $("#edit_ticket_btn").click(function () {
            $("#edit-ticketmodal").modal();
        })

        function activaTab(tab) {
            $('.nav-tabs a[href="#' + tab + '"]').tab('show');
        };

        if ($("#comeback").val() == 1) {
            activaTab('menu-tickets');
            $("#myTicketModel").modal('show');
        } else if ($("#comeback").val() == 2) {
            activaTab('menu-tickets');
        } else if ($("#comeback").val() == "3") {
            activaTab('menu-tickets');
            $("#edit-ticketmodal").modal('show');
        } else {
            activaTab('menu-home');
        }


    });

</script>
<script type="text/javascript">
    //BRON
    //http://www.codexworld.com/select-deselect-all-checkboxes-using-jquery/
    $(document).ready(function () {
        $('#select_all').on('click', function () {
            if (this.checked) {
                $('.checkbox').each(function () {
                    this.checked = true;
                });
            } else {
                $('.checkbox').each(function () {
                    this.checked = false;
                });
            }
        });

        $('.checkbox').on('click', function () {
            if ($('.checkbox:checked').length == $('.checkbox').length) {
                $('#select_all').prop('checked', true);
            } else {
                $('#select_all').prop('checked', false);
            }
        });

        //  $('#user_table').DataTable();
    });
</script>

</body>
</html>