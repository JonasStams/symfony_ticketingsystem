<?php
/**
 * Created by PhpStorm.
 * @Author: Jonas Stams
 * @Reviewer: Koen Castermans
 * Date: 23/03/2016
 * Time: 10:01
 * Code voor language klasse toegevoegd op 5/5/2016 door Koen Castermans
 */
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title> <?php echo $this->lang->line('login_title'); ?></title>
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css"
		  integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">



	<!-- Jquery -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
	<!-- Fonts -->
	<link href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700|Lato:400,100,300,700,900' rel='stylesheet' type='text/css'>
	<!-- Responsiveness -->
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<!-- CSS paginas -->
	<?= link_tag('assets/css/main.css')   ?>
	<?= link_tag('assets/css/animate.css')   ?>

</head>
<body>

    
<div class="headerbar visible-xs-block">
    
<img src="<?php echo base_url('assets/img/pxl.png'); ?>" class="pxllogo" alt="Logo pxl">
    
</div>

<div class="container" id="login-screen">

	<div class="top">
		<img src="<?php echo base_url('assets/img/pxl.png'); ?>" class="logo animated fadeIn res hidden-xs" alt="Logo pxl" >
		<h1 id="title" class="res"><span id="logo"><?php echo $this->lang->line('system_name'); ?></span></h1>

	</div>

	<div class="login-box animated fadeInUp">
		<div class="box-header">
			<h2><?php echo $this->lang->line('login_form_h2'); ?></h2>
		</div>

		<!--Form om taal te kiezen (Koen Castermans)-->
		<?php echo form_open('main/set_language');
		echo form_label("Language");
		echo "<br/>";
		echo form_submit('nederlands', 'Nederlands');
		echo form_submit('english', 'English');
		echo form_close();
		?>
		
		<?php echo form_open('main/login_validation');
			echo validation_errors();
			if(isset($message)){echo $message . '</br>';};
		    echo form_label($this->lang->line('login_form_label_email'));
			echo "<br/>";
			echo form_input('email');
			echo "<br/>";
		echo form_label($this->lang->line('login_form_label_wachtwoord'));
		echo "<br/>";
		echo form_password('password');
		echo "<hr><br/>";
		echo form_submit('login_submit', $this->lang->line('login_form_submit'));
		echo form_close(); ?>


	</div>
</div>
</body>
</html>