<?php
/**
 * Created by PhpStorm.
 * @Author: Sander Jordens
 * @Reviewer: Koen Castermans
 * Date: 1/05/2016
 * Time: 19:28
 * Code voor language klasse toegevoegd door Koen Castermans op 8/5/2016
 */

defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title><?php echo $this->lang->line('view_dispatchers_title'); ?></title>

    <!-- Jquery -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
    <!-- Fonts -->
    <link href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700|Lato:400,100,300,700,900'
          rel='stylesheet' type='text/css'>
    <!-- Responsiveness -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css"
          integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdnmen.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css"
          integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">
    <!-- Pagination -->
    <?= link_tag('https://cdn.datatables.net/1.10.11/css/jquery.dataTables.min.css') ?>
    <script src="https://cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js"></script>
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"
            integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS"
            crossorigin="anonymous"></script>
    <!-- CSS paginas -->
    <?= link_tag('assets/css/main.css') ?>
    <?= link_tag('assets/css/animate.css') ?>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
</head>
<body>
<div class="container-members">
    <div class="row">
        <div class="col-md-3">
        </div>
        <div class="col-md-4">
            <p class="navbar-text navbar-right"><a href='<?php echo base_url() . "members" ?>'><?php echo $this->lang->line('user_panel'); ?></a></p>
        </div>

        <div class="col-md-5">
            <p class="navbar-text navbar-right">
                <?php echo $this->lang->line('signed_in'); ?>
                <a href="#"><?php print_r($this->session->userdata('username')) ?> </a> |
                <a href='<?php echo base_url() . "main/logout" ?>'> <?php echo $this->lang->line('logout'); ?></a>
            </p>
        </div>

        <div class="row">
            <div class="col-md-6">
                <h1><img src='<?php echo base_url() . "assets/img/pxl.png" ?>' class="logo-sm" alt="PXL-logo"/> | <?php echo $this->lang->line('system_name'); ?></h1>
            </div>
        </div>
    </div>

<!-- Einde header, begin van navigatie balk -->


<ul class="nav nav-tabs" style="margin-top: 10px;">
    <li class="active"><a data-toggle="tab" id="tickets_home" href="#open_tickets"><?php echo $this->lang->line('view_dispatchers_tab1_name'); ?></a></li>
    <li><a data-toggle="tab" id="workers_home" href="#worker_list"><?php echo $this->lang->line('view_dispatchers_tab2_name'); ?></a></li>
</ul>

<div class="tab-content">
    <div id="open_tickets" class="tab-pane fade in active">
        <div class="row">

            <div class="col-md-8"><!--col-md-7 naar col-md-8 veranderd door Koen Castermans-->

                <h4><?php echo $this->lang->line('view_dispatchers_h4_open_tickets'); ?></h4>
                <p style="height:35px"><?php echo $this->lang->line('view_dispatchers_info_open_tickets'); ?></p>
                <?php echo form_open('dispatchers/ticket_table_handler'); ?>

                <button class="btn btn-warning" style="margin-bottom:25px" name="sbm" id="asign_ticket_info_btn"
                        value="asign_ticket_info" type="submit">
                    <i class="fa fa-user-plus" aria-hidden="true"></i><?php echo $this->lang->line('view_dispatchers_button_assign_ticket'); ?>
                </button>
                <button class="btn btn-danger" style="margin-bottom:25px" name="sbm"
                        onclick="return confirm('<?php echo $this->lang->line('view_dispatchers_mention_delete'); ?>');"
                        value="delete_tickets" type="submit">
                    <i class="fa fa-trash"></i> <?php echo $this->lang->line('view_dispatchers_button_delete'); ?>
                </button>
                <button class="btn btn-info" style="margin-bottom:25px" name="sbm" value="change_ticket_status"
                        type="submit">
                    <i class="fa fa-pencil-square-o"></i> <?php echo $this->lang->line('view_dispatchers_button_status'); ?>
                </button>
                <button class="btn btn-success" style="margin-bottom:25px" name="sbm" id="get_ticket_info_btn" value="get_ticket_info"
                        type="submit">
                    <i class="fa fa-search"></i> <?php echo $this->lang->line('view_dispatchers_button_info'); ?>
                </button>
            </div>


            <!-- Ticket tabel aanmaken. Aangepast door Sander Jordens op 27/4. Aanklikbare rijen maken. System->libraries->session->table.php -->
            <!-- Aangepast door Sander Jordens op 28/4. Css van Jonas weggedaan. Met css generator een nieuwe gemaakt. -->

            <div class="row ">
                <div class="col-md-12 ">
                    <?php
                    if (isset($message)) {
                        echo "<div class='alert alert-success' role='alert'>" . $message . "</div>";
                    }
                    if (isset($warning)) {
                        echo "<div class='alert alert-warning' role='alert'>" . $warning . "</div>";
                    }
                    if (isset($error)) {
                        echo "<div class='alert alert-danger' role='alert'>" . $error . "</div>";
                    }
                    $this->load->library('table');                
                    $tmpl = array(
                        'table_open' => '<table  class="table  dispat smartph extra">'
                    );
                    $chk_settings = array(
                        'id'          => 'select_all',
                        'checked'     => FALSE
                    );
                    $status='';

                    $tickets[] = array(form_checkbox($chk_settings),$this->lang->line('ticket_table_ticketid'),$this->lang->line('ticket_table_campus'),$this->lang->line('ticket_table_place'),$this->lang->line('ticket_table_description'),$this->lang->line('ticket_table_subject'),$this->lang->line('ticket_table_status'), $this->lang->line('ticket_table_created')); //kolom namen meegeven voor de table

                    foreach($all_tickets as $ticket){
                        /**
                         * Gemaakt door: Jonas Stams
                         * Bijgevoegd in deze view door: Koen Castermans
                         * ------------------------------
                         */
                        switch(strtolower($ticket->campus)) {
                            case "1":
                                $campus = "<span class='label label-primary'>Diepenbeek</span>";
                                break;
                            case "2":
                                $campus = "<span class='label label-info'>Elfde Linie</span>";
                                break;
                            case "3":
                                $campus = "<span class='label label-warning'>Guffenslaan</span>";
                                break;
                            case "4":
                                $campus = "<span class='label label-default'>Quartier Canal</span>";
                                break;
                            case "5":
                                $campus = "<span class='label label-default'>Vildersstraat</span>";
                        }
                        /**
                         * ----------------------------
                         */
                        switch(strtolower($ticket->status)){
                            case "1":
                                $status = "<span class='label label-primary'>Awaiting Operative</span>";
                                break;
                            case "2":
                                $status = "<span class='label label-info'>In Progress</span>";
                                break;
                            case "3":
                                $status = "<span class='label label-warning'>Solved</span>";
                                break;
                            case "4":
                                $status = "<span class='label label-default'>On hold</span>";
                                break;
                            case "5":
                                $status="<span class='label label-default'>Closed</span>";
                        }
                        if(isset($view)){

                        }
                        $chk_settings = array(
                            'name'        => 'select['.$ticket->id.']',
                            'id'          => 'select['.$ticket->id.']',
                            'class'       => 'checkbox',
                            'value'       => $ticket->id,
                            'checked'     => FALSE
                        );
                        //Fout aangepast door Koen Castermans -> campusnaam ipv campuscijfer
                        $tickets[] = array(form_checkbox($chk_settings),$ticket->id, ucfirst($campus),
                            ucfirst($ticket->place), ucfirst($ticket->description),ucfirst($ticket->subject), $status, $ticket->created_on); //tickets in array zetten
                    }
                    //aangepast Pieter& Jonas
                    $message = $this->session->flashdata("message");
                    $warning = $this->session->flashdata("warning");
                    $error = $this->session->flashdata("error");
                    if (isset($message)) {
                        echo "<div class='alert alert-success' role='alter'><strong>Message! </strong>" . $message . "</div>";
                    }
                    if (isset($warning)) {
                        echo "<div class='alert alert-warning' role='alert'><strong>Warning! </strong>" . $warning . "</div>";
                    }
                    if (isset($error)) {
                        echo "<div class='alert alert-danger' role='alert'><strong>Error! </strong>" . $error . "</div>";
                    }
                    $this->table->set_template($tmpl);
                    echo "<div class='table-responsive'>";
                    echo $this->table->generate($tickets);
                    echo form_close();
                    echo '<p>' . $links . '</p>';
                    echo "</div>"; ?>
                </div>
            </div>



            <!-- Sander Jordens 5/5 16:06 :Modal maken voor extra ticket info -->
            <div class="modal fade" id="get_info_modal" role="dialog">
                <div class="modal-dialog">

                    <div class="modal-content" style="text-align: center;">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 style="font-size:1.6em;"><?php echo $this->lang->line('view_dispatchers_modal_info_h4_overview'); ?></h4>
                        </div>
                        <div class="modal-body" style="padding:40px 50px; background-color:white">

                            <h5><?php  echo $this->lang->line('view_dispatchers_modal_info_id') . $info->id?>  </h5><hr>
                            <h5><?php  echo $this->lang->line('view_dispatchers_modal_info_submitted') . $creator->username?>  </h5><hr>
                            <h5><?php echo $this->lang->line('view_dispatchers_modal_info_worker') . $assigned?> </h5><hr>
                            <h5><?php  echo $this->lang->line('view_dispatchers_modal_info_status') . $ticket_status?>  </h5><hr>
                            <h5><?php  echo $this->lang->line('view_dispatchers_modal_info_subject') . $info->subject?>  </h5><hr>
                            <h5><?php  echo $this->lang->line('view_dispatchers_modal_info_description') . $info->description?>  </h5><hr>
                            <h5><?php  echo $this->lang->line('view_dispatchers_modal_info_campus') . $campus_name?>  </h5><hr>
                            <h5><?php  echo $this->lang->line('view_dispatchers_modal_info_place') . $info->place?>  </h5><hr>
                            <h5><?php  echo $this->lang->line('view_dispatchers_modal_info_priority') . $info->priority?>  </h5><hr>
                            <h5><?php  echo $this->lang->line('view_dispatchers_modal_info_creation_date') . $info->created_on?>  </h5><hr>

                        </div>
                    </div>
                </div>
            </div>

<!-- asign ticket modal by Pieter Bollen-->
            <div class="modal fade" id="asign_ticket_modal" role="dialog">
                <div class="modal-dialog">

                    <div class="modal-content" style="text-align: center;">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 style="font-size:1.6em;"><?php echo $this->lang->line('view_dispatchers_modal_info_h4_asign_worker'); ?></h4>
                        </div>
                        <div class="modal-body" style="padding:40px 50px; background-color:white">
                            <?php echo form_open('dispatchers/asign_worker_to_ticket');
                            echo form_hidden('ticket_id', $info->id);
                            ?>
                            <h5><?php  echo $this->lang->line('view_dispatchers_modal_info_id') . $info->id?>  </h5><hr>
                            <h5><?php  echo $this->lang->line('view_dispatchers_modal_info_submitted') . $creator->username?>  </h5><hr>
                            <?php
                                echo "<label for='workersSelect'>" . $this->lang->line('view_dispatchers_modal_info_choose_worker') . "</label>";
                                echo " <select name='workersSelect' id='workersSelect' style='width:50%;'>";
                                foreach ($all_users as $item) {
                                    echo "<option value='". $item->id . "'>Worker : " .   $item->username . "</option>";
                                }
                                echo "</select></br><hr>";

                            ?>

                            <h5><?php  echo $this->lang->line('view_dispatchers_modal_info_status') . $ticket_status?>  </h5><hr>
                            <h5><?php  echo $this->lang->line('view_dispatchers_modal_info_subject') . $info->subject?>  </h5><hr>
                            <h5><?php  echo $this->lang->line('view_dispatchers_modal_info_description') . $info->description?>  </h5><hr>
                            <h5><?php  echo $this->lang->line('view_dispatchers_modal_info_campus') . $campus_name?>  </h5><hr>
                            <h5><?php  echo $this->lang->line('view_dispatchers_modal_info_place') . $info->place?>  </h5><hr>
                            <h5><?php  echo $this->lang->line('view_dispatchers_modal_info_priority') . $info->priority?>  </h5><hr>
                            <h5><?php  echo $this->lang->line('view_dispatchers_modal_info_creation_date') . $info->created_on?>  </h5><hr>
                            <h5><button class="btn btn-success" style="margin-bottom:25px" name="sbm" id="add_Worker" value="update_ticket_worker"
                                        type="submit">
                                    <i class="fa fa-search"></i> <?php echo $this->lang->line('view_dispatchers_button_assign_worker'); ?>
                                </button></h5>
                           <?   echo form_submit('edit_ticket',"Choose worker");
                                echo form_close(); ?>

                        </div>



                    </div>
                </div>
            </div>



        </div>
    </div>




<!-- worker table -->

     <div id="worker_list" class="tab-pane fade in">
        <div class="col-md-7">
            <h4><?php echo $this->lang->line('view_dispatchers_h4_available_workers'); ?></h4>
            <p style="height:35px"><?php echo $this->lang->line('view_dispatchers_info_available_workers'); ?></p>
            <?php echo form_open('dispatchers/user_table_handler'); ?>


            </div>
        <div class="row ">
            <div class="col-md-12 ">
                <?php
                if (isset($message)) {
                    echo "<div class='alert alert-success' role='alert'>" . $message . "</div>";
                }
                if (isset($warning)) {
                    echo "<div class='alert alert-warning' role='alert'>" . $warning . "</div>";
                }
                if (isset($error)) {
                    echo "<div class='alert alert-danger' role='alert'>" . $error . "</div>";
                }
                $this->load->library('table');
                $tmpl = array(
                    'table_open' => '<table class="table  dispat smartph extra">',
                    'heading_row_start' => "<tr>", 'row_alt_start' => '<tr href=\'  <?php echo base_url()."views/dispatchers/ticketSummery"?>   \'>',
                );
                $chk_settings = array(
                    'id'          => 'select_all',
                    'checked'     => FALSE
                );
                $users[] = array(form_checkbox($chk_settings),$this->lang->line('worker_table_id'), $this->lang->line('worker_table_username'), $this->lang->line('worker_table_email'), $this->lang->line('worker_table_role'), $this->lang->line('worker_table_status')); //kolom namen meegeven voor de table
                foreach($all_users as $user){
                    if($user->banned == 0){
                        $activated = "<span class='label label-success'>Actief</span>";
                    }else{
                        $activated = "<span class='label label-danger'>Geblokkeerd</span>";
                    }
                    switch(strtolower($user->user_role)){
                        case "1":
                            $role = "<span class='label label-primary'>Admin</span>";
                            break;
                        case "2":
                            $role = "<span class='label label-info'>User</span>";
                            break;
                        case "3":
                            $role = "<span class='label label-warning'>Dispatcher</span>";
                            break;
                        case "4":
                            $role = "<span class='label label-default'>Werkman</span>";
                            break;
                    }

                    $chk_settings = array(
                        'name'        => 'select['.$user->id.']',
                        'id'          => 'select['.$user->id.']',
                        'class'       => 'checkbox',
                        'value'       => $user->id,
                        'checked'     => FALSE
                    );

                    if(isset($view)){
                        $value = $view;
                    }else{
                        $value = 0;
                    }
                    $comeback = array(
                        'name'        => 'comeback',
                        'id'          => 'comeback',
                        'value'          => $value,
                        'type'          => 'hidden',
                        'maxlength'   => '1',
                        'size'        => '1',

                    );
                    echo form_input($comeback);
                    $users[] = array(form_checkbox($chk_settings),$user->id, ucfirst($user->username), ucfirst($user->email), $role, $activated); //users in array zetten
                }
                $this->table->set_template($tmpl);
                echo "<div class='table-responsive'>";
                echo $this->table->generate($users);

                echo form_close();
                echo "</div>";; ?>
            </div>
        </div>
        </div>
    </div>




    </div>

</div>
</div>


</body>


<script>
    $(document).ready(function () {
       function activaTab(tab) {
            $('.nav-tabs a[href="#' + tab + '"]').tab('show');
        }
        var comeback = $("#comeback").val;
        switch (comeback){
            case 1:
                $("#asign_ticket_modal").modal('show');
                break;
        }
        if ($("#comeback").val() == 1) {
            activaTab('open_tickets');
            $("#asign_ticket_modal").modal('show');
        } else if ($("#comeback").val() == 2) {
            activaTab('open_tickets');
        } else if ($("#comeback").val() == 3) {
            activaTab('open_tickets');
            $("#asign_ticket_modal").modal('show');
        } else if ($("#comeback").val() == 4) {
            activaTab('open_tickets');
            $("#get_info_modal").modal('show');
        } else {
            activaTab('open_tickets');
        }


    });

</script>
<script type="text/javascript">
    //BRON
    //http://www.codexworld.com/select-deselect-all-checkboxes-using-jquery/
    $(document).ready(function () {
        $('#select_all').on('click', function () {
            if (this.checked) {
                $('.checkbox').each(function () {
                    this.checked = true;
                });
            } else {
                $('.checkbox').each(function () {
                    this.checked = false;
                });
            }
        });

        $('.checkbox').on('click', function () {
            if ($('.checkbox:checked').length == $('.checkbox').length) {
                $('#select_all').prop('checked', true);
            } else {
                $('#select_all').prop('checked', false);
            }
        });

        $('#user_table').DataTable();
    });
</script>

</html>