<?php
/**
 * Created by PhpStorm.
 * @Author: Jonas Stams
 * @Reviewer:
 * Date: 23/03/2016
 * Time: 18:43
 * Code voor language klasse toegevoegd op 7/5/2016 door Koen Castermans
 */
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title><?php echo $this->lang->line('error_title'); ?></title>
    <!-- CSS paginas -->
    <?= link_tag('assets/css/main.css')   ?>
    <?= link_tag('assets/css/animate.css')   ?>
    <!-- Jquery -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
    <!-- Fonts -->
    <link href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700|Lato:400,100,300,700,900' rel='stylesheet' type='text/css'>
    <!-- Responsiveness -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

</head>
<body>
<div class="container">
    <h1><?php echo $this->lang->line('error_h1_no_access'); ?></h1>

    <a href="<?php echo base_url()."main/login"; ?>"><?php echo $this->lang->line('error_back'); ?></a>
</div>
</div>
</body>
</html>