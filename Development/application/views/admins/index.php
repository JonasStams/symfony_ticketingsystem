<?php
/**
 * Created by PhpStorm.
 * @Author: Jonas Stams
 * @Reviewer: Koen Castermans
 * Date: 24/03/2016
 * Time: 15:36
 * Code toegevoegd voor language klasse toegevoegd op 5/5/2016 door Koen Castermans
 */

defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title><?php echo $this->lang->line('admins_index_title'); ?></title>

    <!-- Jquery -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
    <!-- Fonts -->
    <link href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700|Lato:400,100,300,700,900' rel='stylesheet' type='text/css'>
    <!-- Responsiveness -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">
    <!-- Pagination -->
    <?= link_tag('https://cdn.datatables.net/1.10.11/css/jquery.dataTables.min.css')   ?>
    <script src="https://cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js"></script>
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
    <!-- CSS paginas -->
    <?= link_tag('assets/css/main.css')   ?>
    <?= link_tag('assets/css/animate.css')   ?>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
</head>
<body>
<div class="container-members">
    <div class="row">
        <div class="col-md-4">
        </div>
        <div class="col-md-4">
            <p class="navbar-text navbar-right"><a href='<?php echo base_url()."members"?>'> <?php echo $this->lang->line('user_panel'); ?> </a></p>

        </div>
        <div class="col-md-4">
            <p class="navbar-text navbar-right">
                <?php echo $this->lang->line('signed_in'); ?>
                <a href="#"><?php print_r($this->session->userdata('username')) ?></a>
                |
                <a href='<?php echo base_url()."main/logout"?>'> <?php echo $this->lang->line('logout'); ?> </a>
            </p>

        </div>

        <div class="row">
            <div class="col-md-6">

                <h1><img src='<?php echo base_url()."assets/img/pxl.png"?>' class="logo-sm" alt="Logo van PXL" /> | <?php echo $this->lang->line('system_name'); ?></h1>
            </div>

        </div>



            <div id="menu-users">

                <div class="row">
                    <div class="col-md-6">
                        <h3><?php echo $this->lang->line('admins_index_h3_users'); ?></h3>
                        <p><?php echo $this->lang->line('admins_index_info_users'); ?></p>
                    </div>
                    <div class="col-md-6">
                        <form class="navbar-form navbar-right" role="search">
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Search user">
                            </div>
                            <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                        </form>
                    </div>

                </div>
                <div class="row">
                    <div class="col-md-5">
                        <h4><?php echo $this->lang->line('admins_index_h4_new_user'); ?></h4>
                        <p style="height:35px"><?php echo $this->lang->line('admins_index_info_new_user') ?></p>
                        <button class="btn btn-success" style="margin-bottom:25px" id="create_user_btn" type="button">
                            <i class="fa fa-plus"></i> <?php echo $this->lang->line('admins_index_button_add'); ?>
                        </button>
                    </div>
                    <div class="col-md-7">
                        <h4><?php echo $this->lang->line('admins_index_h4_check_users') ?></h4>
                        <p style="height:35px"><?php echo $this->lang->line('admins_index_info_check_users'); ?></p>
                        <?php echo form_open('admins/user_table_handler'); ?>
                        <button class="btn btn-warning" style="margin-bottom:25px" name="sbm" value="block_unblock" type="submit">
                            <i class="fa fa-ban"></i> <?php echo $this->lang->line('admins_index_button_(un)block'); ?>
                        </button>
                        <button class="btn btn-danger" style="margin-bottom:25px" name="sbm"  onclick="return confirm('<?php echo $this->lang->line('admins_index_mention_delete') ?>');" value="delete_users" type="submit">
                            <i class="fa fa-trash"></i> <?php echo $this->lang->line('admins_index_button_delete'); ?>
                        </button>
                        <button class="btn btn-info" style="margin-bottom:25px" name="sbm"  value="edit_user" type="submit">
                            <i class="fa fa-pencil-square-o"></i> <?php echo $this->lang->line('admins_index_button_modify'); ?>
                        </button>

                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <?php
                            if(isset($message)){
                                echo   "<div class='alert alert-success' role='alert'>" . $message . "</div>";
                            }
                            if(isset($warning)){
                                echo   "<div class='alert alert-warning' role='alert'>" . $warning . "</div>";
                            }
                            if(isset($error)){
                                echo   "<div class='alert alert-danger' role='alert'>" . $error . "</div>";
                            }
                            $this->load->library('table');                //inladen van library voor tables
                            $chk_settings = array(
                                'id'          => 'select_all',
                                'checked'     => FALSE
                            );
                            $users[] = array(form_checkbox($chk_settings),$this->lang->line('user_table_id'), $this->lang->line('user_table_username'), $this->lang->line('user_table_email'), $this->lang->line('user_table_role'), $this->lang->line('user_table_status')); //kolom namen meegeven voor de table
                            foreach($all_users as $user){
                                if($user->banned == 0){
                                    $activated = "<span class='label label-success'>Actief</span>";
                                }else{
                                    $activated = "<span class='label label-danger'>Geblokkeerd</span>";
                                }
                                switch(strtolower($user->user_role)){
                                    case "1":
                                        $role = "<span class='label label-primary'>Admin</span>";
                                        break;
                                    case "2":
                                        $role = "<span class='label label-info'>User</span>";
                                        break;
                                    case "3":
                                        $role = "<span class='label label-default'>Dispatcher</span>";
                                        break;
                                    case "4":
                                        $role = "<span class='label label-warning'>Werkman</span>";
                                        break;
                                }

                                $chk_settings = array(
                                    'name'        => 'select['.$user->id.']',
                                    'id'          => 'select['.$user->id.']',
                                    'class'       => 'checkbox',
                                    'value'       => $user->id,
                                    'checked'     => FALSE
                                );
                                $users[] = array(form_checkbox($chk_settings),$user->id, ucfirst($user->username), ucfirst($user->email), $role, $activated); //users in array zetten
                            }
                            $tmpl = array(
                                'table_open' => '<table id="user_table"   class="table table-striped user smartph extra">',
                                'heading_row_start' => "<tr style='height: 45px;background-color: #C7E9A4;'>"
                            );
                            $this->table->set_template($tmpl);
                            echo "<div class='table-responsive'>";
                            echo $this->table->generate($users);
                            echo form_close();
                            echo "<p> $links </p>";
                            echo "</div>";
                            ;?>

                        </div>
                    </div>
                    <div class="modal fade" id="myModal" role="dialog">
                        <div class="modal-dialog">

                            <div class="modal-content" style="text-align: center;">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 style="font-size:1.6em;"><?php echo $this->lang->line('admins_index_modal_h4_create_user') ?></h4>
                                </div>
                                <div class="modal-body" style="padding:40px 50px; background-color:white">

                                    <?php echo form_open('admins/create_user');
                                    $email = array(
                                        'name'        => 'email',
                                        'id'          => 'email',
                                        'type'          => 'email',
                                        'maxlength'   => '100',
                                        'size'        => '50',
                                        'style'       => 'width:50%',
                                    );
                                    $username = array(
                                        'name'        => 'username',
                                        'id'          => 'username',
                                        'maxlength'   => '100',
                                        'size'        => '50',
                                        'style'       => 'width:50%',
                                    );
                                    $password = array(
                                        'name'        => 'password',
                                        'id'          => 'password',
                                        'type'          => 'password',
                                        'maxlength'   => '100',
                                        'size'        => '50',
                                        'style'       => 'width:50%',
                                    );
                                    $conf_password = array(
                                        'name'        => 'conf_password',
                                        'id'          => 'conf_password',
                                        'type'          => 'password',
                                        'maxlength'   => '100',
                                        'size'        => '50',
                                        'style'       => 'width:50%',
                                    );
                                    if(isset($view)){
                                        $value = $view;
                                    }else{
                                        $value = 0;
                                    }
                                    $comeback = array(
                                        'name'        => 'comeback',
                                        'id'          => 'comeback',
                                        'value'          => $value,
                                        'type'          => 'hidden',
                                        'maxlength'   => '1',
                                        'size'        => '1',

                                    );

                                    echo form_input($comeback);
                                    echo validation_errors();
                                    echo form_label($this->lang->line('admins_index_modal_label_email'));
                                    echo "<br/>";
                                    echo form_input($email);
                                    echo "<br/>";
                                    echo form_label($this->lang->line('admins_index_modal_label_username'));
                                    echo "<br/>";
                                    echo form_input($username);
                                    echo "<br/>";
                                    echo form_label($this->lang->line('admins_index_modal_label_password'));
                                    echo "<br/>";
                                    echo form_password($password);
                                    echo "<br/>";
                                    echo form_label($this->lang->line('admins_index_modal_label_password_confirm'));
                                    echo "<br/>";
                                    echo form_password($conf_password);
                                    echo "<br/>";
                                    echo form_label($this->lang->line('admins_index_modal_label_role'));
                                    echo "<br/>";
                                    ?>
                                    <select name='user_role' id='user_role' style="width:50%">
                                        <option value='1'>Admin</option>
                                        <option value='2' selected='selected'>User</option>
                                        <option value='3'>Dispatcher</option>
                                        <option value='4'>Werkman</option>
                                    </select>
                                    <?php
                                    if(isset($message)){
                                        echo   "<div class='alert alert-succes' role='alter'>" . $message . "</p>";
                                    }
                                    if(isset($warning)){
                                        echo   "<div class='alert alert-warning' role='alert'>" . $warning . "</div>";
                                    }
                                    if(isset($error)){
                                        echo   "<div class='alert alert-danger' role='alert'>" . $error . "</div>";
                                    }

                                    echo "<br/>";
                                    echo form_submit('create_submit', $this->lang->line('admins_index_modal_submit_save'), "class='btn btn-success'");
                                    echo form_close();?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal fade" id="edit-modal" role="dialog">
                        <div class="modal-dialog">

                            <div class="modal-content" style="text-align: center;">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 style="font-size:1.6em;"><?php echo $this->lang->line('admins_index_modal_h4_modify_user') ?></h4>
                                </div>
                                <div class="modal-body" style="padding:40px 50px; background-color:white">
                                    <h5><?php echo $this->lang->line('admins_index_modal_h5_modify_password'); ?></h5><hr>
                                    <?php echo form_open('admins/change_password');

                                    $password = array(
                                        'name'        => 'password',
                                        'id'          => 'password',
                                        'type'          => 'password',
                                        'maxlength'   => '100',
                                        'size'        => '50',
                                        'style'       => 'width:50%',
                                    );
                                    $conf_password = array(
                                        'name'        => 'conf_password',
                                        'id'          => 'conf_password',
                                        'type'          => 'password',
                                        'maxlength'   => '100',
                                        'size'        => '50',
                                        'style'       => 'width:50%',
                                    );
                                    if(isset($view)){
                                        $value = $view;
                                    }else{
                                        $value = 0;
                                    }
                                    $comeback = array(
                                        'name'        => 'comeback',
                                        'id'          => 'comeback',
                                        'value'          => $value,
                                        'type'          => 'hidden',
                                        'maxlength'   => '1',
                                        'size'        => '1',

                                    );
                                    echo validation_errors();
                                    echo form_input($comeback);
                                    echo form_hidden('user_id',$user_id);
                                    echo validation_errors();
                                    echo form_label($this->lang->line('admins_index_modal_label_new_password'));
                                    echo "<br/>";
                                    echo form_password($password);
                                    echo "<br/>";
                                    echo form_label($this->lang->line('admins_index_modal_label_new_password_confirm'));
                                    echo "<br/>";
                                    echo form_password($conf_password);
                                    echo "<br/>";

                                    if(isset($message)){
                                        echo   "<div class='alert alert-succes' role='alter'>" . $message . "</p>";
                                    }
                                    if(isset($warning)){
                                        echo   "<div class='alert alert-warning' role='alert'>" . $warning . "</div>";
                                    }
                                    if(isset($error)){
                                        echo   "<div class='alert alert-danger' role='alert'>" . $error . "</div>";
                                    }
                                    echo "<br/>";
                                    echo form_submit('change_pw', $this->lang->line('admins_index_modal_submit_change'));
                                    echo form_close();?>

                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

</body>
<script>
    $(document).ready(function(){
        $("#create_user_btn").click(function(){
            $("#myModal").modal();
        });
        $("#create_user_menu").click(function(){
            $("#myModal").modal();
        });

        function activaTab(tab){
            $('.nav-tabs a[href="#' + tab + '"]').tab('show');
        };

        if($("#comeback").val() == 1) {
            activaTab('menu-users');
            $("#myModal").modal('show');
        }else if($("#comeback").val() == 2) {
            activaTab('menu-users');
        }else if($("#comeback").val() == 3){
            activaTab('menu-users');
            $("#edit-modal").modal('show');
        }else{
            activaTab('menu-home');
        }
    });

</script>
<script type="text/javascript">
    //BRON
    //http://www.codexworld.com/select-deselect-all-checkboxes-using-jquery/
    $(document).ready(function(){
        $('#select_all').on('click',function(){
            if(this.checked){
                $('.checkbox').each(function(){
                    this.checked = true;
                });
            }else{
                $('.checkbox').each(function(){
                    this.checked = false;
                });
            }
        });

        $('.checkbox').on('click',function(){
            if($('.checkbox:checked').length == $('.checkbox').length){
                $('#select_all').prop('checked',true);
            }else{
                $('#select_all').prop('checked',false);
            }
        });

        //   $('#user_table').DataTable();
    });
</script>
</html>