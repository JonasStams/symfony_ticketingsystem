<?php
/**
 * Created by PhpStorm.
 * @Author: Sander Jordens
 * @Reviewer: Koen Castermans
 * Date: 1/05/2016
 * Time: 19:28
 * language klasse toegevoegd door Koen Castermans op 9/5/2016
 */

defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title><?php echo $this->lang->line('view_workers_title'); ?></title>

    <!-- Jquery -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
    <!-- Fonts -->
    <link href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700|Lato:400,100,300,700,900'
          rel='stylesheet' type='text/css'>
    <!-- Responsiveness -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css"
          integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdnmen.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css"
          integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">
    <!-- Pagination -->
    <?= link_tag('https://cdn.datatables.net/1.10.11/css/jquery.dataTables.min.css') ?>
    <script src="https://cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js"></script>
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"
            integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS"
            crossorigin="anonymous"></script>
    <!-- CSS paginas -->
    <?= link_tag('assets/css/main.css') ?>
    <?= link_tag('assets/css/animate.css') ?>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
</head>
<body>
<div class="container-members">
    <div class="row">
        <div class="col-md-4"><!-- col aangepast voor tekst door Koen Castermans -->
        </div>
        <div class="col-md-4">
            <p class="navbar-text navbar-right"><a href='<?php echo base_url() . "members" ?>'><?php echo $this->lang->line('user_panel'); ?></a></p>
        </div>

        <div class="col-md-4">
            <p class="navbar-text navbar-right">
                <?php echo $this->lang->line('signed_in'); ?>
                <a href="#"><?php print_r($this->session->userdata('username')) ?></a> |
                <a href='<?php echo base_url() . "main/logout" ?>'> <?php echo $this->lang->line('logout'); ?></a>
            </p>
        </div>

        <div class="row">
            <div class="col-md-6">
                <h1><img src='<?php echo base_url() . "assets/img/pxl.png" ?>' alt="Logo pxl" class="logo-sm"/> | <?php echo $this->lang->line('system_name'); ?></h1>
            </div>
        </div>

        <!-- Einde header, begin van navigatie balk -->


        <div id="menu-tickets">

            <div class="row">
                <div class="col-md-6">
                    <h3><?php echo $this->lang->line('view_workers_h3_tickets'); ?></h3>
                </div>
                <div class="col-md-6">
                    <form class="navbar-form navbar-right" role="search">
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="<?php echo $this->lang->line('search_ticket'); ?>">
                        </div>
                        <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                    </form>
                </div>
            </div>

            <div class="row">

                <div class="col-md-4">
                    <p><?php echo $this->lang->line('view_workers_info_ticket_status'); ?></p>
                    <?php
                    if (isset($ticket_awaiting_count)) echo "<span class='label label-primary col-md-9 status_count'><span class='badge'>" . $ticket_awaiting_count . "</span>" . $this->lang->line('view_workers_status_awaiting') . "</span>";
                    if (isset($ticket_in_progress_count)) echo "<span class='label label-info col-md-9 status_count'><span class='badge'>" . $ticket_in_progress_count . "</span>" . $this->lang->line('view_workers_status_in_progress') . "</span>";
                    if (isset($ticket_on_hold_count)) echo "<span class='label label-default col-md-9 status_count'><span class='badge'>" . $ticket_on_hold_count . "</span>" . $this->lang->line('view_workers_status_on_hold') . "</span>";
                    if (isset($ticket_solved_count)) echo "<span class='label label-warning col-md-9 status_count'><span class='badge'>" . $ticket_solved_count . "</span>" . $this->lang->line('view_workers_status_solved') . "</span>";
                    if (isset($ticket_closed_count)) echo "<span class='label label-danger col-md-9 status_count'><span class='badge'>" . $ticket_closed_count . "</span>" . $this->lang->line('view_workers_status_closed') . "</span>";
                    ?>
                </div>

                <div class="col-md-7">

                    <p style="height:35px"><?php echo $this->lang->line('view_workers_info_change_status'); ?></p>
                    <?php echo form_open('workers/ticket_table_handler'); ?>
                    <button class="btn btn-success" style="margin-bottom:25px" name="sbm" value="new_message"
                            type="submit">
                        <i class="fa fa-envelope"></i> <?php echo $this->lang->line('view_workers_button_new_message'); ?>
                    </button>
                    <button class="btn btn-primary" style="margin-bottom:25px" name="sbm" value="chk_messages"
                            type="submit">
                        <i class="fa fa-envelope"></i> <?php echo $this->lang->line('view_workers_button_check_messages'); ?>
                    </button>
                    <br/>
                    <button class="btn btn-info" style="margin-bottom:25px" name="sbm" value="in_progress"
                            type="submit">
                        <i class="fa fa-pencil-square-o"></i> <?php echo $this->lang->line('view_workers_button_set_in_progress'); ?>
                    </button>
                    <button class="btn btn-warning" style="margin-bottom:25px" name="sbm" value="on_hold" type="submit">
                        <i class="fa fa-pencil-square-o"></i> <?php echo $this->lang->line('view_workers_button_set_on_hold'); ?>
                    </button>
                    <button class="btn btn-danger" style="margin-bottom:25px" name="sbm" value="closed" type="submit">
                        <i class="fa fa-pencil-square-o"></i> <?php echo $this->lang->line('view_workers_button_set_closed'); ?>
                    </button>
                </div>
            </div>
            <hr/>
            <br/>
            <!-- Ticket tabel aanmaken. Aangepast door Sander Jordens op 27/4. Aanklikbare rijen maken. System->libraries->session->table.php -->
            <!-- Aangepast door Sander Jordens op 28/4. Css van Jonas weggedaan. Met css generator een nieuwe gemaakt. -->

            <div class="row ">
                <div class="col-md-12 ">
                    <?php

                    $this->load->library('table');                //Inladen van Table.php uit library voor tables -- weggenomen -->table table-striped
                    $tmpl = array(
                        'table_open' => '<table  class="CSSTableGenerator">',
                        'heading_row_start' => "<tr>", 'row_alt_start' => '<tr class=\'clickable-row\' href=\'  <?php echo base_url()."views/dispatchers/ticketSummery"?>   \'>',
                    );

                    if (isset($ticket_awaiting_count)) {
                        echo "<div class='alert alert-danger' role='alert'>";
                        echo "<strong>" . $this->lang->line('view_workers_warning') . "</strong>". $this->lang->line('view_workers_warning_there_are') . "<span class='badge'>" . $ticket_awaiting_count . "</span>" . $this->lang->line('view_workers_warning_ticket_approvement') . "</div>";
                    }

                    if ($all_tickets) {


                        $chk_settings = array(
                            'id' => 'select_all',
                            'checked' => FALSE
                        );
                        $status = '';
                        $tickets[] = array(form_checkbox($chk_settings), '#', $this->lang->line('ticket_table_worker_id'), $this->lang->line('ticket_table_campus'), $this->lang->line('ticket_table_place'), $this->lang->line('ticket_table_subject'), $this->lang->line('ticket_table_description'),$this->lang->line('ticket_table_status') , $this->lang->line('ticket_table_created')); //kolom namen meegeven voor de table
                        foreach ($all_tickets as $ticket) {

                            switch (strtolower($ticket->campus)) {
                                case "1":
                                    $campus = "<span class='label label-primary'>Diepenbeek</span>";
                                    break;
                                case "2":
                                    $campus = "<span class='label label-info'>Elfde Linie</span>";
                                    break;
                                case "3":
                                    $campus = "<span class='label label-warning'>Guffenslaan</span>";
                                    break;
                                case "4":
                                    $campus = "<span class='label label-default'>Quartier Canal</span>";
                                    break;
                                case "5":
                                    $campus = "<span class='label label-danger'>Vildersstraat</span>";
                            }
                            switch (strtolower($ticket->status)) {
                                case "1":
                                    $status = "<span class='label label-primary'>Awaiting Operative</span>";
                                    break;
                                case "2":
                                    $status = "<span class='label label-info'>In Progress</span>";
                                    break;
                                case "3":
                                    $status = "<span class='label label-default'>On hold</span>";
                                    break;
                                case "4":
                                    $status = "<span class='label label-warning'>Solved</span>";
                                    break;
                                case "5":
                                    $status = "<span class='label label-danger'>Closed</span>";
                            }

                            $chk_settings = array(
                                'name' => 'select[' . $ticket->id . ']',
                                'id' => 'select[' . $ticket->id . ']',
                                'class' => 'checkbox',
                                'value' => $ticket->id,
                                'checked' => FALSE
                            );
                            $tickets[] = array(form_checkbox($chk_settings), $ticket->id, $ticket->assigned_to, ucfirst($campus),
                                ucfirst($ticket->place), ucfirst($ticket->description), ucfirst($ticket->subject), $status, $ticket->created_on); //tickets in array zetten
                        }
                        $this->table->set_template($tmpl);
                        echo "<div class='table-responsive'>";
                        echo $this->table->generate($tickets);
                        echo form_close();
                        echo "<p> $links </p>";
                        echo "</div>";
                    }
                    if (isset($message)) {
                        echo "<div class='alert alert-success' role='alert'>" . $message . "</div>";
                    }
                    if (!empty($this->session->flashdata('message'))) {
                        echo "<div class='alert alert-success' role='alert'>" . $this->session->flashdata('message') . "</div>";
                    }

                    if (isset($warning)) {
                    echo "<div class='alert alert-warning' role='alert'>" . $warning . "</div>";
                    }
                    if (!empty($this->session->flashdata('warning'))) {
                        echo "<div class='alert alert-warning' role='alert'>" . $this->session->flashdata('warning') . "</div>";
                    }

                    if (isset($error)) {
                    echo "<div class='alert alert-danger' role='alert'>" . $error . "</div>";
                    }
                    if (!empty($this->session->flashdata('error'))) {
                        echo "<div class='alert alert-danger' role='alert'>" . $this->session->flashdata('error') . "</div>";
                    } ?>
                </div>
            </div>


            <div class="modal fade" id="messages-modal" role="dialog">
                <div class="modal-dialog">

                    <div class="modal-content" style="text-align: center;">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 style="font-size:1.6em;"><?php echo $this->lang->line('view_workers_modal_check_h4'); ?></h4>
                        </div>
                        <div class="modal-body" style="padding:40px 50px; background-color:white">
                            <h5><?php echo $this->lang->line('view_workers_modal_check&create_id') . $ticket_id ?></h5>
                            <h6><?php echo $this->lang->line('view_workers_modal_check&create_description') . $ticketdata['description'] ?></h6>
                            <hr>
                            <?php

                            if(!empty($ticket_messages)){
                                $count = 1;
                                foreach ($ticket_messages as $row){ ?>
                                    <div class="panel panel-success">
                                        <div class="panel-heading">
                                            <h3 class="panel-title"><?php echo $this->lang->line('view_workers_modal_check_message_nr') .  $count++;?></h3>
                                        </div>
                                        <div class="panel-body">
                                            <h4></h4>
                                            <p><strong><?php echo $this->lang->line('view_workers_modal_check_from'); ?></strong><?php echo "user " . $row['user_id']; ?></p><hr/>
                                            <p><strong><?php echo $this->lang->line('view_workers_modal_check_created'); ?></strong><?php $date = date_create($row['created']); echo date_format($date,DATE_COOKIE );?></p>
                                            <hr/>
                                            <p><strong><?php echo $this->lang->line('view_workers_modal_check_message'); ?></strong> <?php echo $row['message']; ?></p>
                                        </div>
                                    </div>
                              <?php  }
                            }

                            ?>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal fade" id="new-message-modal" role="dialog">
                <div class="modal-dialog">

                    <div class="modal-content" style="text-align: center;">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 style="font-size:1.6em;"><?php echo $this->lang->line('view_workers_modal_create_h4'); ?></h4>
                        </div>
                        <div class="modal-body" style="padding:40px 50px; background-color:white">
                            <h5><?php echo $this->lang->line('view_workers_modal_check&create_id') . $ticket_id ?></h5>
                            <h6><?php echo $this->lang->line('view_workers_modal_check&create_description') . $ticketdata['description'] ?></h6>
                            <hr>
                            <?php echo form_open('workers/create_message');

                            $conf_password = array(
                                'name' => 'conf_password',
                                'id' => 'conf_password',
                                'type' => 'password',
                                'maxlength' => '100',
                                'size' => '50',
                                'style' => 'width:50%',
                            );
                            if (isset($view)) {
                                $value = $view;
                            } else {
                                $value = 0;
                            }
                            $comeback = array(
                                'name' => 'comeback',
                                'id' => 'comeback',
                                'value' => $value,
                                'type' => 'hidden',
                                'maxlength' => '1',
                                'size' => '1',

                            );
                            echo form_hidden("ticket_id", $ticket_id);
                            echo form_input($comeback);
                            echo validation_errors();
                            echo form_label($this->lang->line('view_workers_modal_create_label_message'));
                            echo "<br/>";
                            echo form_textarea('message');
                            echo "<br/>";

                            if (isset($message)) {
                                echo "<div class='alert alert-succes' role='alert'>" . $message . "</p>";
                            }
                            if (isset($warning)) {
                                echo "<div class='alert alert-warning' role='alert'>" . $warning . "</div>";
                            }
                            if (isset($error)) {
                                echo "<div class='alert alert-danger' role='alert'>" . $error . "</div>";
                            }
                            echo "<br/>";
                            echo form_submit('change_pw', $this->lang->line('view_workers_modal_create_submit_create'));
                            echo form_close(); ?>

                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>


</div>
</div>

</body>
<script>
    $(document).ready(function () {
        $("#create_user_btn").click(function () {
            $("#myModal").modal();
        });
        $("#create_user_menu").click(function () {
            $("#myModal").modal();
        });

        function activaTab(tab) {
            $('.nav-tabs a[href="#' + tab + '"]').tab('show');
        };

        if ($("#comeback").val() == 1) {
            $("#myModal").modal('show');
        } else if ($("#comeback").val() == 2) {
            $("#messages-modal").modal('show');
        } else if ($("#comeback").val() == 3) {
            $("#new-message-modal").modal('show');
        } else {
            activaTab('menu-home');
        }


    });

</script>
<script type="text/javascript">
    //BRON
    //http://www.codexworld.com/select-deselect-all-checkboxes-using-jquery/
    $(document).ready(function () {
        $('#select_all').on('click', function () {
            if (this.checked) {
                $('.checkbox').each(function () {
                    this.checked = true;
                });
            } else {
                $('.checkbox').each(function () {
                    this.checked = false;
                });
            }
        });

        $('.checkbox').on('click', function () {
            if ($('.checkbox:checked').length == $('.checkbox').length) {
                $('#select_all').prop('checked', true);
            } else {
                $('#select_all').prop('checked', false);
            }
        });

        $('#user_table').DataTable();
    });
</script>

</html>