<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * @author Pieter Bollen
 * @reviewer: Koen Castermans
 * messages en errors aangepast voor language klasse door Koen Castermans op 7/5/2016
 */



class Members extends CI_Controller
{

    public function __construct() {
        parent:: __construct();
        $this->load->helper("url");
        $this->load->model("tickets");
        $this->load->library("pagination");
        //taal bepalen
        $this->site_language->load_language();;//Inladen van language file toegevoegd door Koen Castermans op 14/5/2016
    }

    public function index()
    {
        if ($this->session->userdata('is_logged_in')) {
            $config = $this->add_pagination();
            $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
            $data['all_tickets'] = $this->tickets->get_all_tickets_by_creator_pagination($this->session->userdata('id'),$config["per_page"], $page);      //user table in $data['users'] zetten, deze stuur ik mee naar de view en kan daarna $users gebruiken in de view
            $data['links'] = $this->pagination->create_links();
            $config["total_rows"] = count($data['all_tickets']);
            $this->pagination->initialize($config);
            $this->load->view('members/members', $data);

        } else {
            redirect('main/restricted');
        }
    }


    public function create_ticket()
    {
        $this->load->helper('form');          //helper classes inladen
        $this->load->library('form_validation');              //model users inladen
        $this->load->model('tickets');
        //validatie van formulier
        $this->form_validation->set_rules('place', 'place', 'required');
        $this->form_validation->set_rules('subject', 'subject', 'required');
        $this->form_validation->set_rules('description', 'description', 'required');

        if ($this->form_validation->run()) {

            $ticket = array(                                                              //array met alle waardes voor de nieuwe gebruiker
                'campus' => $this->input->post('campus'),
                'place' => $this->input->post('place'),
                'subject' => $this->input->post('subject'),
                'description' => $this->input->post('description'),
                'created_on' => date('Y-m-d H:i:s'),
                'created_by' => $this->session->userdata('id'),
                'status' => '1'
            );
            if( $this->tickets->create_ticket($ticket)){
                $this->session->set_flashdata('message',$this->lang->line('controller_members_message_ticket_created'));
            }
            else{
                $this->session->set_flashdata('message',$this->lang->line('controller_members_message_ticket_created_fail'));
            }

            redirect('members/index');

        }


    }
    /* function get_ticket_info_by_id($id)
     {

         $this->db->select('tickets.campus', 'tickets.place', 'tickets.subject', 'tickets.description');
         $this->db->from('tickets');
         $this->db->where('tickets.id', $id);
         $query = $this->db->get($this->table_name);
         if ($query->num_rows() == 1) return $query->row();
         return NULL;




     }*/


    /** Author: pieter bollen
     * Reviewer: Jonas Stams
     *
     */

    public function update_ticket(){
        $ticket_id = $this->input->post('ticket_id');
        $campus= $this->input->post('campus');
        $place= $this->input->post('place');
        $subject= $this->input->post('subject');
        $description= $this->input->post('description');
        $this->tickets->update_ticket($ticket_id, $campus, $place, $subject, $description );
        $this->session->set_flashdata('message', $this->lang->line('controller_members_message_ticket_updated'));
        redirect('members/index');
    }




    /**
     * @author Pieter Bollen
     */
    public function ticket_table_handler()
    {
        // $this->load->model('tickets', 'tickets');
        $delete = $this->input->post('delete');
        $selected = $this->input->post('select');
        if($this->input->post('sbm') == 'delete_tickets'){
            if(isset($selected)){
                foreach($selected as $id){

                    $this->tickets->delete_ticket($id);
                }

                $this->session->set_flashdata('message', $this->lang->line('controller_members_message_ticket_deleted'));

                redirect('members/index');
            }else{

                $this->session->set_flashdata('error',$this->lang->line('controller_members_error_delete_select'));
                redirect('members/index');

            }
        }


        if($this->input->post('sbm') == 'edit_ticket'){
            if(isset($selected)){
                if(count($selected) == 1){
                    $id = reset($selected);
                    $data = array('ticket_id' => $id, 'view' => 3);

                    $config = $this->add_pagination();
                    $this->pagination->initialize($config);
                    $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
                    $data['links'] = $this->pagination->create_links();

                    $data['all_tickets'] = $this->tickets->get_all_tickets_by_creator_pagination($this->session->userdata('id'),$config["per_page"], $page);      //user table in $data['users'] zetten, deze stuur ik mee naar de view en kan daarna $users gebruiken in de view


                    $ticket = $this->tickets->get_ticket_info_by_id($id);

                    $data['ticketdata']['campus']= $ticket->campus;
                    $data['ticketdata']['description']= $ticket->description;
                    $data['ticketdata']['subject']= $ticket->subject;
                    $data['ticketdata']['place']= $ticket->place;

                    $this->load->view('members/members', $data);
                }else if(isset($selected)){{
                    if(count($selected)>1){
                        $this->session->set_flashdata('error', $this->lang->line('controller_members_error_edit_one_ticket'));
                        redirect('members/index');
                    }
                }
                }
            }else{
                $this->session->set_flashdata('error',$this->lang->line('controller_members_error_edit_no_selection'));

                redirect('members/index');
            }
        }
    }

    /**
     * @author Jonas Stams
     * @reviewer: Pieter Bollen
     *
     */
    public function add_pagination(){
        $config["base_url"] = base_url() . "members/index";
        $config["total_rows"] = $this->tickets->ticket_count();
        $config["per_page"] = 5;
        $config["uri_segment"] = 3;

        return $config;
    }




}