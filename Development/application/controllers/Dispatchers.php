<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * @author Pieter Bollen
 */



    class Dispatchers extends CI_Controller
    {

        public function __construct() {
            parent:: __construct();
            $this->load->helper("url");
            $this->load->model("campusses");
            $this->load->model("tickets");
            $this->load->model("users");
            $this->load->model("workers");
            $this->load->model('ticket_statusses'); // aangemaakt door Sander 7/5
            $this->load->library("pagination");
            //taal bepalen
            $this->site_language->load_language(); //Toegevoegd door Koen Castermans op 14/5/2016
        }

        /**
         * @author
         * @reviewer
         */
        public function index()
        {
            if ($this->session->userdata('is_logged_in')) {
                $config = $this->add_pagination();
                $this->pagination->initialize($config);
                $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;

                $data['all_tickets'] = $this->tickets->get_all_tickets_without_worker($config["per_page"], $page);        //user table in $data['users'] zetten, deze stuur ik mee naar de view en kan daarna $users gebruiken in de view
                $data['all_users'] = $this->users->get_all_users_by_status(4);//Status van 3 naar 4 aangepast door Koen Castermans -> anders werden de dispatcher geladen in plaats van de werkmannen

                $data['links'] = $this->pagination->create_links();
                $this->load->view('dispatchers/dispatchers', $data);

            }
        }

        /**
         * @author Jonas Stams
         * @reviewer
         * @param	int, int
         * @return	array
         */

        /**
         * @author Jonas Stams
         * @reviewer
         * @return	array
         */
        public function add_pagination(){
            $config["base_url"] = base_url() . "dispatchers/index";
            $config["total_rows"] = $this->tickets->count_all_tickets_without_worker();
            $config["per_page"] = 5;
            $config["uri_segment"] = 3;

            return $config;
        }

        /**
         * @author Pieter Bollen
         * @reviewer
         */
        public function update_ticket(){
            $ticket_id = $this->input->post('ticket_id');
            $campus= $this->input->post('campus');
            $place= $this->input->post('place');
            $subject= $this->input->post('subject');
            $description= $this->input->post('description');
            $worker=$this->input->post('worker');
            $this->tickets->update_ticket($ticket_id, $campus, $place, $subject, $description,$worker);
            $this->session->set_flashdata('message', $this->lang->line('controller_dispatchers_message_ticket_updated'));
            redirect('dispatchers/index');
        }



        /**
         * @author Pieter Bollen
         * @reviewer
         * @return	array
         */
        public function update_ticket_status(){
            $ticket_id=$this->input->post('ticket_id');
            $status=$this->input->post('status');
            $this->tickets->update_ticket_status($ticket_id,$status);
        }

        /**
         * @author Pieter Bollen
         * @reviewer
         */
        public function update_ticket_worker(){
            $ticket_id=$this->input->post('ticket_id');
            $worker=$this->input->post('worker');
            $this->tickets->update_ticket_worker($ticket_id,$worker);
        }

        /**
         * @author Pieter Bollen
         * @reviewer
         */
        public function user_table_handler()
        {
            $delete = $this->input->post('delete');
            $selected = $this->input->post('select');
            if($this->input->post('sbm') == 'delete_users'){
                if(isset($selected)){
                    foreach($selected as $id){
                        $this->users->delete_user($id);
                    }
                    $data = array('message' => 'Gebruikers zijn verwijderd.', 'view' => 2);
                    $config = $this->add_pagination();

                    $this->pagination->initialize($config);
                    $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
                    $data['all_tickets'] = $this->tickets->get_all_tickets_without_worker($config["per_page"], $page);        //user table in $data['users'] zetten, deze stuur ik mee naar de view en kan daarna $users gebruiken in de view
                    $data['all_users'] = $this->users->get_all_users_by_status(3);
                    $data['links'] = $this->pagination->create_links();
                    $this->load->view('dispatchers/index', $data);
                }else{
                    $data = array('Error' => 'U heeft geen gebruiker geselecteerd.', 'view' => 2);
                    $config = $this->add_pagination();

                    $this->pagination->initialize($config);
                    $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
                    $data['all_tickets'] = $this->tickets->get_all_tickets_without_worker($config["per_page"], $page);        //user table in $data['users'] zetten, deze stuur ik mee naar de view en kan daarna $users gebruiken in de view
                    $data['all_users'] = $this->users->get_all_users_by_status(3);
                    $data['links'] = $this->pagination->create_links();
                    $this->load->view('dispatchers/index', $data);
                }
            }
            if($this->input->post('sbm') == 'block_unblock'){
                if(isset($selected)){
                    foreach($selected as $id){
                        if($this->users->check_ban_status($id) == '0'){
                            $this->users->ban_user($id);
                        }else{
                            $this->users->unban_user($id);
                        }

                    }
                    $data = array('message' => 'User(s) zijn ge(de)blokkeerd.', 'view' => 2);
                    $config = $this->add_pagination();

                    $this->pagination->initialize($config);
                    $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
                    $data['all_tickets'] = $this->tickets->get_all_tickets_without_worker($config["per_page"], $page);        //user table in $data['users'] zetten, deze stuur ik mee naar de view en kan daarna $users gebruiken in de view
                    $data['all_users'] = $this->users->get_all_users_by_status(3);
                    $data['links'] = $this->pagination->create_links();
                    $this->load->view('dispatchers/index', $data);
                }else{
                    $data = array('error' => 'U heeft geen gebruiker geselecteerd.', 'view' => 2);
                    $config = $this->add_pagination();

                    $this->pagination->initialize($config);
                    $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
                    $data['all_tickets'] = $this->tickets->get_all_tickets_without_worker($config["per_page"], $page);        //user table in $data['users'] zetten, deze stuur ik mee naar de view en kan daarna $users gebruiken in de view
                    $data['all_users'] = $this->users->get_all_users_by_status(3);
                    $data['links'] = $this->pagination->create_links();
                    $this->load->view('dispatchers/index', $data);
                }
            }

            if($this->input->post('sbm') == 'edit_user'){
                if(isset($selected)){
                    if(count($selected) == 1){
                        $id = reset($selected);
                        $data = array('user_id' => $id, 'view' => 3);
                        $config = $this->add_pagination();

                        $this->pagination->initialize($config);
                        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
                        $data['all_tickets'] = $this->tickets->get_all_tickets_without_worker($config["per_page"], $page);        //user table in $data['users'] zetten, deze stuur ik mee naar de view en kan daarna $users gebruiken in de view
                        $data['all_users'] = $this->users->get_all_users_by_status(3);
                        $data['links'] = $this->pagination->create_links();
                        $this->load->view('dispatchers/index', $data);
                    }else{
                        $data = array('error' => 'U kan maar één user per keer aanpassen.', 'view' => 2);
                        $config = $this->add_pagination();

                        $this->pagination->initialize($config);
                        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
                        $data['all_tickets'] = $this->tickets->get_all_tickets_without_worker($config["per_page"], $page);        //user table in $data['users'] zetten, deze stuur ik mee naar de view en kan daarna $users gebruiken in de view
                        $data['all_users'] = $this->users->get_all_users_by_status(3);
                        $data['links'] = $this->pagination->create_links();
                        $this->load->view('dispatchers/index', $data);
                    }
                }else{
                    $data = array('error' => 'U moet een user selecteren.', 'view' => 2);
                    $config = $this->add_pagination();
                    $this->pagination->initialize($config);
                    $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
                    $data['all_tickets'] = $this->tickets->get_all_tickets_without_worker($config["per_page"], $page);        //user table in $data['users'] zetten, deze stuur ik mee naar de view en kan daarna $users gebruiken in de view
                    $data['all_users'] = $this->users->get_all_users_by_status(3);
                    $data['links'] = $this->pagination->create_links();
                    $this->load->view('dispatchers/index', $data);
                }
            }
        }

        /**
         * Author: Jonas Stams
         * Revieuwer: Pieter Bollen
         */
        public function asign_worker_to_ticket(){
           $worker_id =  $this->input->post('workersSelect');
           $ticket_id =  $this->input->post('ticket_id');

            if($this->tickets->update_ticket_assigned_to($ticket_id,$worker_id)){
                $this->session->set_flashdata('message', 'Assigned worker!');
                redirect('dispatchers/index');
            }else{
                $this->session->set_flashdata('error', 'No worker assigned');
                redirect('dispatchers/index');
            }
        }

        /**
         * @author Pieter Bollen
         * @reviewer Jonas Stams
         */

        public function ticket_table_handler()
        {
            // $this->load->model('tickets', 'tickets');
            $delete = $this->input->post('delete');
            $selected = $this->input->post('select');
            if($this->input->post('sbm') == 'delete_tickets'){
                if(isset($selected)){
                    foreach($selected as $id){

                        $this->tickets->delete_ticket($id);
                    }

                    $this->session->set_flashdata('message', $this->lang->line('controller_dispatchers_message_ticket_deleted'));

                    redirect('dispatchers/index');
                }else{

                    $this->session->set_flashdata('error',"You didn't select any ticket(s) to delete");
                    redirect('dispatchers/index');

                }
            }

            /**
             * @author Pieter Bollen, Jonas Stams
             * @reviewer Jonas Stams
             */
            if($this->input->post('sbm') == 'asign_ticket_info'){
                if(isset($selected)){
                    if(count($selected) == 1){
                        $id = reset($selected);
                        $data = array('ticket_id' => $id, 'view' => 3);
                        $config = $this->add_pagination();
                        $this->pagination->initialize($config);
                        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
                        $data['all_tickets'] = $this->tickets->get_all_tickets_without_worker($config["per_page"], $page);        //user table in $data['users'] zetten, deze stuur ik mee naar de view en kan daarna $users gebruiken in de view
                        $data['all_users'] = $this->users->get_all_users_by_status(4);
                        $data['links'] = $this->pagination->create_links();
                        $data['info']=$this->tickets->get_ticket_info_by_id($id);
                        $id_creator = $data['info']->created_by;   // Kan dit niet efficienter?
                        $id_worker = $data['info']->assigned_to;
                        $status_id = $data['info']->status;
                        $campus_id= $data['info']->campus;
                        $data['creator'] = $this->users->get_user_by_id($id_creator, true);
                        $data['ticket_status'] = $this->ticket_statusses->get_description_by_id($status_id);
                        $data['assigned'] = $this->workers->get_worker_id_by_user_id($id_worker);
                       if ($data['assigned'] == null) {
                            $data['assigned'] = $this->workers->get_all_workers();
                        }

                     $data['campus_name'] = $this->campusses->get_description_by_id($campus_id);


                        $this->load->view('dispatchers/dispatchers', $data);

                    }else if(isset($selected)){{
                        if(count($selected)>1){
                            $this->session->set_flashdata('error', 'You can only view 1 ticket at a time.');
                            redirect('dispatchers/dispatchers');
                        }
                    }
                    }
                }else{
                    $this->session->set_flashdata('error','U have to select a ticket in order to use this option.');
                    redirect('dispatchers/dispatchers');
                }
            }

            /**
             * @author Sander Jordens
             * @reviewer Pieter Bollen, Jonas Stams
             */
            //Gemaakt door Sander Jordens 5/5 om 16:09
            if($this->input->post('sbm') == 'get_ticket_info'){
                if(isset($selected)){
                    if(count($selected) == 1){
                        //$this-> load->model('tickets');    //mag weg
                        $id = reset($selected);
                        $data = array('ticket_id' => $id, 'view' => 4);
                        $config = $this->add_pagination();
                        $this->pagination->initialize($config);
                        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
                        $data['all_tickets'] = $this->tickets->get_all_tickets_without_worker($config["per_page"], $page);        //user table in $data['users'] zetten, deze stuur ik mee naar de view en kan daarna $users gebruiken in de view
                        $data['all_users'] = $this->users->get_all_users_by_status(3);
                        $data['links'] = $this->pagination->create_links();
                        $data['info']=$this->tickets->get_ticket_info_by_id($id);
                        $id_creator = $data['info']->created_by;   // Kan dit niet efficienter?
                        $id_worker = $data['info']->assigned_by;
                        $status_id = $data['info']->status;
                        $campus_id= $data['info']->campus;
                        $data['creator'] = $this->users->get_user_by_id($id_creator, true);
                        $data['ticket_status'] = $this->ticket_statusses->get_description_by_id($status_id);
                        $data['assigned'] = $this->workers->get_worker_id_by_user_id($id_worker);
                        if ($data['assigned'] == NUll){
                            $data['assigned'] = 'Not assigned';}
                        $data['campus_name'] = $this->campusses->get_description_by_id($campus_id);


                        $this->load->view('dispatchers/dispatchers', $data);

                    }else if(isset($selected)){{
                        if(count($selected)>1){
                            $this->session->set_flashdata('error', 'You can only view 1 ticket at a time.');
                            redirect('dispatchers/dispatchers');
                        }
                    }
                    }
                }else{
                    $this->session->set_flashdata('error','U have to select a ticket in order to use this option.');
                    redirect('dispatchers/dispatchers');
                }
            }




            }

        }