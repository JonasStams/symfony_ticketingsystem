<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * @Author: Jonas Stams
 * @Reviewer:
 * Date: 24/03/2016
 * Time: 15:34
 * Code voor language klasse toegevoegd op 5/5/2016 door Koen Castermans
 */

class Admins extends CI_Controller
{
    /**
     * @author Jonas Stams
     * @reviewer: Koen Castermans
     */
    public function __construct() {
        parent:: __construct();
        $this->load->helper("url");
        $this->load->model("users");
        $this->load->library("pagination");

        //taal bepalen
        $this->site_language->load_language();
    }
    /**
     * @author Jonas Stams
     * @reviewer: Koen Castermans
     */
    public function index()
    {
        if ($this->session->userdata('user_role') == 1) {   //check of user een admin is
            $config = $this->add_pagination();

            $this->pagination->initialize($config);
            $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
            $data['all_users'] = $this->users->get_all_users_pagination($config["per_page"], $page);      //user table in $data['users'] zetten, deze stuur ik mee naar de view en kan daarna $users gebruiken in de view
            $data['links'] = $this->pagination->create_links();
            $data['view'] = 2;
            $this->load->view('admins/index', $data);       //view openen en data mee sturen
        } else {
            redirect('main/restricted');                    //als de user geen admin, krijgt hij restricted pagina
        }
    }
    /**
     * @Author: Jonas Stams
     * @Reviewer: Koen Castermans
     */
    public function create_user()
    {
        $this->load->helper('form');          //helper classes inladen
        $this->load->library('form_validation');              //model users inladen

        //validatie van formulier
        $this->form_validation->set_rules('username', 'Username', 'required|is_unique[users.username]');
        $this->form_validation->set_rules('password', 'Password', 'required|min_length[7]');
        $this->form_validation->set_rules('conf_password', 'Password Confirmation', 'required');
        $this->form_validation->set_rules('email', 'Email', 'required|trim|valid_email|is_unique[users.email]');

        if ($this->form_validation->run()) {
            if($this->input->post('password') == $this->input->post('conf_password')){      //Bekijken of de ingegeven passwoorden hetzelfde zijn
                $hash = $this->bcrypt->hash_password($this->input->post('password'));       //Bcrypt functie om het passwpord veilig te hashen voor het in de DB geschreven wordt.
                $data = array(                                                              //array met alle waardes voor de nieuwe gebruiker
                    'email' => $this->input->post('email'),
                    'username' => $this->input->post('username'),
                    'password' => $hash,
                    'user_role' => $this->input->post('user_role')
                );
                $this->users->create_user($data);                                           //functie uit user model, hiermee schrijf ik de user in de DB
                if($this->input->post('create_submit') == 'Save'){
                    $data = array('message' => $this->lang->line('controller_admins_message_user_added'), 'view' => 2);
                    $config = $this->add_pagination();
                    $this->pagination->initialize($config);
                    $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
                    $data['all_users'] = $this->users->get_all_users_pagination($config["per_page"], $page);      //user table in $data['users'] zetten, deze stuur ik mee naar de view en kan daarna $users gebruiken in de view
                    $data['links'] = $this->pagination->create_links();
                    $this->load->view('admins/index', $data);
                }else{
                    redirect('admins/user_created');                      //redirect naar andere action in deze controller
                }

            }else{
                redirect('admins/form_password_error');                  //redirect naar andere action in deze controller
            }

        } else {
            redirect('admins/form_validation_fail');                     //redirect naar andere action in deze controller

        }
    }
    /**
     * @Author: Jonas Stams
     * @Reviewer: Koen Castermans
     */
    public function user_table_handler()
    {
        $delete = $this->input->post('delete');
        $selected = $this->input->post('select');
        if($this->input->post('sbm') == 'delete_users'){
            if(isset($selected)){
                foreach($selected as $id){
                    $this->users->delete_user($id);
                }
                $data = array('message' => $this->lang->line('controller_admins_message_user_deleted'), 'view' => 2);
                $config = $this->add_pagination();

                $this->pagination->initialize($config);
                $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
                $data['all_users'] = $this->users->get_all_users_pagination($config["per_page"], $page);      //user table in $data['users'] zetten, deze stuur ik mee naar de view en kan daarna $users gebruiken in de view
                $data['links'] = $this->pagination->create_links();
                $this->load->view('admins/index', $data);
            }else{
                $data = array('error' => $this->lang->line('controller_admins_error_user_not_selected'), 'view' => 2);
                $config = $this->add_pagination();

                $this->pagination->initialize($config);
                $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
                $data['all_users'] = $this->users->get_all_users_pagination($config["per_page"], $page);      //user table in $data['users'] zetten, deze stuur ik mee naar de view en kan daarna $users gebruiken in de view
                $data['links'] = $this->pagination->create_links();
                $this->load->view('admins/index', $data);
            }
        }
        if($this->input->post('sbm') == 'block_unblock'){
            if(isset($selected)){
                foreach($selected as $id){
                    if($this->users->check_ban_status($id) == '0'){
                        $this->users->ban_user($id);
                    }else{
                        $this->users->unban_user($id);
                    }
                }
                $data = array('message' => $this->lang->line('controller_admins_message_user_(un)blocked'), 'view' => 2);
                $config = $this->add_pagination();

                $this->pagination->initialize($config);
                $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
                $data['all_users'] = $this->users->get_all_users_pagination($config["per_page"], $page);      //user table in $data['users'] zetten, deze stuur ik mee naar de view en kan daarna $users gebruiken in de view
                $data['links'] = $this->pagination->create_links();
                $this->load->view('admins/index', $data);
            }else{
                $data = array('error' => $this->lang->line('controller_admins_error_user_not_selected'), 'view' => 2);
                $config = $this->add_pagination();

                $this->pagination->initialize($config);
                $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
                $data['all_users'] = $this->users->get_all_users_pagination($config["per_page"], $page);      //user table in $data['users'] zetten, deze stuur ik mee naar de view en kan daarna $users gebruiken in de view
                $data['links'] = $this->pagination->create_links();
                $this->load->view('admins/index', $data);
            }
        }

        if($this->input->post('sbm') == 'edit_user'){  // Knop: 'Aanpassen'
            if(isset($selected)){
                if(count($selected) == 1){
                    $id = reset($selected);
                    $data = array('user_id' => $id, 'view' => 3);
                    $config = $this->add_pagination();

                    $this->pagination->initialize($config);
                    $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
                    $data['all_users'] = $this->users->get_all_users_pagination($config["per_page"], $page);      //user table in $data['users'] zetten, deze stuur ik mee naar de view en kan daarna $users gebruiken in de view
                    $data['links'] = $this->pagination->create_links();
                    $this->load->view('admins/index', $data);
                }else{
                    $data = array('error' => $this->lang->line('controller_admins_error_modify_one_user'), 'view' => 2);
                    $config = $this->add_pagination();

                    $this->pagination->initialize($config);
                    $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
                    $data['all_users'] = $this->users->get_all_users_pagination($config["per_page"], $page);      //user table in $data['users'] zetten, deze stuur ik mee naar de view en kan daarna $users gebruiken in de view
                    $data['links'] = $this->pagination->create_links();
                    $this->load->view('admins/index', $data);
                }
            }else{
                $data = array('error' => $this->lang->line('controller_admins_error_select_user'), 'view' => 2);
                $config = $this->add_pagination();
                $this->pagination->initialize($config);
                $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
                $data['all_users'] = $this->users->get_all_users_pagination($config["per_page"], $page);      //user table in $data['users'] zetten, deze stuur ik mee naar de view en kan daarna $users gebruiken in de view
                $data['links'] = $this->pagination->create_links();
                $this->load->view('admins/index', $data);
            }
        }
    }
    /**
     * @Author: Jonas Stams
     * @Reviewer: Koen Castermans
     */
    public function change_password(){
        $user_id = $this->input->post('user_id');
        if($this->input->post('password') == $this->input->post('conf_password')){
            $hash = $this->bcrypt->hash_password($this->input->post('password'));
            $this->users->change_password($user_id,$hash);
            $data = array('message' => $this->lang->line('controller_admins_message_password_changed'), 'view' => 2);
            $config = $this->add_pagination();

            $this->pagination->initialize($config);
            $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
            $data['all_users'] = $this->users->get_all_users_pagination($config["per_page"], $page);      //user table in $data['users'] zetten, deze stuur ik mee naar de view en kan daarna $users gebruiken in de view
            $data['links'] = $this->pagination->create_links();
            $this->load->view('admins/index', $data);

        }else{
            $data = array('error' => $this->lang->line('controller_admins_error_no_identical_pass'), 'view' => 3, 'user_id' => $user_id);
            $config = $this->add_pagination();

            $this->pagination->initialize($config);
            $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
            $data['all_users'] = $this->users->get_all_users_pagination($config["per_page"], $page);      //user table in $data['users'] zetten, deze stuur ik mee naar de view en kan daarna $users gebruiken in de view
            $data['links'] = $this->pagination->create_links();
            $this->load->view('admins/index', $data);
        }
    }
    /**
     * @Author: Jonas Stams
     * @Reviewer: Koen Castermans
     */
    public function form_validation_fail(){
        $data = array('error' => $this->lang->line('controller_admins_error_already_used'), 'view' => 1);
        $config = $this->add_pagination();

        $this->pagination->initialize($config);
        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $data['all_users'] = $this->users->get_all_users_pagination($config["per_page"], $page);      //user table in $data['users'] zetten, deze stuur ik mee naar de view en kan daarna $users gebruiken in de view
        $data['links'] = $this->pagination->create_links();
        $this->load->view('admins/index', $data);
    }
    /**
     * @Author: Jonas Stams
     * @Reviewer: Koen Castermans
     */
    public function form_password_error(){
        $data = array('error' => $this->lang->line('controller_admins_error_different_passwords'), 'view' => 1);
        $config = $this->add_pagination();

        $this->pagination->initialize($config);
        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $data['all_users'] = $this->users->get_all_users_pagination($config["per_page"], $page);      //user table in $data['users'] zetten, deze stuur ik mee naar de view en kan daarna $users gebruiken in de view
        $data['links'] = $this->pagination->create_links();
        $this->load->view('admins/index', $data);
    }
    /**
     * @Author: Jonas Stams
     * @Reviewer: Koen Castermans
     */
    public function user_created(){
        $data = array('message' => $this->lang->line('controller_admins_message_user_added'), 'view' => 1);
        $config = $this->add_pagination();

        $this->pagination->initialize($config);
        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $data['all_users'] = $this->users->get_all_users_pagination($config["per_page"], $page);      //user table in $data['users'] zetten, deze stuur ik mee naar de view en kan daarna $users gebruiken in de view
        $data['links'] = $this->pagination->create_links();
        $this->load->view('admins/index', $data);
    }
    /**
     * @Author: Jonas Stams
     * @Reviewer: Koen Castermans
     */
    public function user_deleted(){
        $data = array('message' => $this->lang->line('controller_admins_message_user_deleted'), 'view' => 2);
       /* $config = $this->add_pagination();

        $this->pagination->initialize($config);
        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $data['users'] = $this->get_user_table($config["per_page"], $page);       //user table in $data['users'] zetten, deze stuur ik mee naar de view en kan daarna $users gebruiken in de view
       */ $data['links'] = $this->pagination->create_links();
        redirect('admins/index');
       // $this->load->view('admins/index', $data);
    }
    /**
     * @Author: Jonas Stams
     * @Reviewer: Koen Castermans
     */
    public function users_deleted(){
        $data = array('message' => $this->lang->line('controller_admins_message_multiple_users_deleted'), 'view' => 2);
        $config = $this->add_pagination();

        $this->pagination->initialize($config);
        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $data['all_users'] = $this->users->get_all_users_pagination($config["per_page"], $page);      //user table in $data['users'] zetten, deze stuur ik mee naar de view en kan daarna $users gebruiken in de view
        $data['links'] = $this->pagination->create_links();
        $this->load->view('admins/index', $data);
    }



    /**
     * @author Jonas Stams
     * @reviewer
     * @return	array
     */
    public function add_pagination(){
        $config["base_url"] = base_url() . "admins/index";
        $config["total_rows"] = $this->users->user_count();
        $config["per_page"] = 5;
        $config["uri_segment"] = 3;

        return $config;
    }
}