<?php

/**
 * Created by PhpStorm.
 * User: Jonas
 * Date: 4/05/2016
 * Time: 16:01
 */
class Workers extends CI_Controller
{
    public function __construct()
    {
        parent:: __construct();
        $this->load->helper("url");
        $this->load->model("tickets");
        $this->load->model("ticket_messages");
        $this->load->library("pagination");
        $this->lang->load('ned', 'nederlands');

        //taal bepalen
        $this->site_language->load_language();
    }

    /**
     * @Author: Jonas Stams
     * @Reviewer:
     */
    public function index()
    {
        if ($this->session->userdata('user_role') == 4) {   //check of user een worker is
            $config = $this->add_pagination();

            $this->pagination->initialize($config);
            $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
            $data['all_tickets'] =  $this->tickets->get_all_tickets_by_worker_id($this->session->userdata('worker_id'), $config["per_page"], $page);   //alle tickets in een variabele zetten
            //user table in $data['users'] zetten, deze stuur ik mee naar de view en kan daarna $users gebruiken in de view
            $data['links'] = $this->pagination->create_links();
            $data['ticket_awaiting_count'] = $this->tickets->count_all_tickets_by_status_and_worker_id(1, $this->session->userdata['worker_id']);
            $data['ticket_in_progress_count'] = $this->tickets->count_all_tickets_by_status_and_worker_id(2, $this->session->userdata['worker_id']);
            $data['ticket_on_hold_count'] = $this->tickets->count_all_tickets_by_status_and_worker_id(3, $this->session->userdata['worker_id']);
            $data['ticket_solved_count'] = $this->tickets->count_all_tickets_by_status_and_worker_id(4, $this->session->userdata['worker_id']);
            $data['ticket_closed_count'] = $this->tickets->count_all_tickets_by_status_and_worker_id(5, $this->session->userdata['worker_id']);

            $this->load->view('workers/workers', $data);;       //view openen en data mee sturen
        } else {
            redirect('main/restricted');                    //als de user geen admin, krijgt hij restricted pagina
        }
    }

    /**
     * @Author: Jonas Stams
     */
    public function add_pagination()
    {
        $config["base_url"] = base_url() . "workers/index";
        $config["total_rows"] = $this->tickets->ticket_count();
        $config["per_page"] = 10;
        $config["uri_segment"] = 3;

        return $config;
    }

    /**
     * @Author: Jonas Stams
     *
     */
    public function create_message()
    {
        $data['user_id'] = $this->session->userdata('id');
        $data['ticket_id'] = $this->input->post('ticket_id');
        $data['message'] = $this->input->post('message');
        $data['created'] = date('Y-m-d H:i:s');
        if ($this->ticket_messages->create_message($data)) {
            $this->session->set_flashdata('message', $this->lang->line('controller_workers_message_new_message_correct'));
        } else {
            $this->session->set_flashdata('error', $this->lang->line('controller_workers_error_new_message_fail'));
        }
        redirect('workers/index');
    }

    /**
     * @author: Jonas Stams
     * @Review: Pieter Bollen, Jonas Stams
     */
    public function ticket_table_handler()
    {
        $selected = $this->input->post('select');


        if ($this->input->post('sbm') == 'new_message') {
            if (isset($selected)) {
                if (count($selected) == 1) {
                    $id = reset($selected);
                    $data = array('ticket_id' => $id, 'view' => 3);

                    $config = $this->add_pagination();
                    $this->pagination->initialize($config);
                    $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
                    $data['links'] = $this->pagination->create_links();

                    $data['all_tickets'] =  $this->tickets->get_all_tickets_by_worker_id($this->session->userdata('worker_id'), $config["per_page"], $page);   //alle tickets in een variabele zetten


                    $ticket = $this->tickets->get_ticket_info_by_id($id);

                    $data['ticketdata']['campus'] = $ticket->campus;
                    $data['ticketdata']['description'] = $ticket->description;
                    $data['ticketdata']['subject'] = $ticket->subject;
                    $data['ticketdata']['place'] = $ticket->place;
                    $data['ticket_awaiting_count'] = $this->tickets->count_all_tickets_by_status_and_worker_id(1, $this->session->userdata['worker_id']);
                    $data['ticket_in_progress_count'] = $this->tickets->count_all_tickets_by_status_and_worker_id(2, $this->session->userdata['worker_id']);
                    $data['ticket_on_hold_count'] = $this->tickets->count_all_tickets_by_status_and_worker_id(3, $this->session->userdata['worker_id']);
                    $data['ticket_solved_count'] = $this->tickets->count_all_tickets_by_status_and_worker_id(4, $this->session->userdata['worker_id']);
                    $data['ticket_closed_count'] = $this->tickets->count_all_tickets_by_status_and_worker_id(5, $this->session->userdata['worker_id']);

                    $this->load->view('workers/workers', $data);
                } else if (isset($selected)) {
                    {
                        if (count($selected) > 1) {
                            $this->session->set_flashdata('error', $this->lang->line('controller_workers_error_edit_one_ticket'));
                            redirect('workers/index');
                        }
                    }
                }
            } else {
                $this->session->set_flashdata('error', $this->lang->line('controller_workers_error_select_ticket'));

                redirect('members/index');
            }


        }else if ($this->input->post('sbm') == 'chk_messages') {
            if (isset($selected)) {
                if (count($selected) == 1) {
                    $id = reset($selected);
                    $data = array('ticket_id' => $id, 'view' => 2);

                    $config = $this->add_pagination();
                    $this->pagination->initialize($config);
                    $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
                    $data['links'] = $this->pagination->create_links();

                    $data['all_tickets'] =  $this->tickets->get_all_tickets_by_worker_id($this->session->userdata('worker_id'), $config["per_page"], $page);   //alle tickets in een variabele zetten
                    $all_messages = $this->ticket_messages->get_all_messages_per_ticket_id($id);

                    foreach ($all_messages as $message) {
                        $data['ticket_messages'][] = array('id' => $message->id,
                                                            'user_id' => $message->user_id,
                                                            'message' =>      $message->message,
                                                            'created' => $message->created); //messages in array zetten
                    }

                    $ticket = $this->tickets->get_ticket_info_by_id($id);

                    $data['ticketdata']['campus'] = $ticket->campus;
                    $data['ticketdata']['description'] = $ticket->description;
                    $data['ticketdata']['subject'] = $ticket->subject;
                    $data['ticketdata']['place'] = $ticket->place;
                    $data['ticket_awaiting_count'] = $this->tickets->count_all_tickets_by_status_and_worker_id(1, $this->session->userdata['worker_id']);
                    $data['ticket_in_progress_count'] = $this->tickets->count_all_tickets_by_status_and_worker_id(2, $this->session->userdata['worker_id']);
                    $data['ticket_on_hold_count'] = $this->tickets->count_all_tickets_by_status_and_worker_id(3, $this->session->userdata['worker_id']);
                    $data['ticket_solved_count'] = $this->tickets->count_all_tickets_by_status_and_worker_id(4, $this->session->userdata['worker_id']);
                    $data['ticket_closed_count'] = $this->tickets->count_all_tickets_by_status_and_worker_id(5, $this->session->userdata['worker_id']);

                    $this->load->view('workers/workers', $data);
                } else if (isset($selected)) {
                    {
                        if (count($selected) > 1) {
                            $this->session->set_flashdata('error', $this->lang->line('controller_workers_error_edit_one_ticket'));
                            redirect('workers/index');
                        }
                    }
                }
            } else {
                $this->session->set_flashdata('error', $this->lang->line('controller_workers_error_select_ticket'));

                redirect('workers/index');
            }
        }else if ($this->input->post('sbm') == 'in_progress') {
            if (isset($selected)) {
                foreach ($selected as $id) {
                    $this->tickets->change_status($id, 2);
                }

                $config = $this->add_pagination();
                $this->pagination->initialize($config);
                $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
                $data['links'] = $this->pagination->create_links();

                $data['all_tickets'] =  $this->tickets->get_all_tickets_by_worker_id($this->session->userdata('worker_id'), $config["per_page"], $page);   //alle tickets in een variabele zetten


                $ticket = $this->tickets->get_ticket_info_by_id($id);

                $data['ticketdata']['campus'] = $ticket->campus;
                $data['ticketdata']['description'] = $ticket->description;
                $data['ticketdata']['subject'] = $ticket->subject;
                $data['ticketdata']['place'] = $ticket->place;
                $data['ticket_awaiting_count'] = $this->tickets->count_all_tickets_by_status_and_worker_id(1, $this->session->userdata['worker_id']);
                $data['ticket_in_progress_count'] = $this->tickets->count_all_tickets_by_status_and_worker_id(2, $this->session->userdata['worker_id']);
                $data['ticket_on_hold_count'] = $this->tickets->count_all_tickets_by_status_and_worker_id(3, $this->session->userdata['worker_id']);
                $data['ticket_solved_count'] = $this->tickets->count_all_tickets_by_status_and_worker_id(4, $this->session->userdata['worker_id']);
                $data['ticket_closed_count'] = $this->tickets->count_all_tickets_by_status_and_worker_id(5, $this->session->userdata['worker_id']);

                $this->load->view('workers/workers', $data);
            } else if (isset($selected)) {
                {
                    if (count($selected) > 1) {
                        $this->session->set_flashdata('error', $this->lang->line('controller_workers_error_edit_one_ticket'));
                        redirect('members/index');
                    }
                }
            }
        }else if ($this->input->post('sbm') == 'on_hold') {
            if (isset($selected)) {
                foreach ($selected as $id) {
                    $this->tickets->change_status($id, 3);
                }

                $config = $this->add_pagination();
                $this->pagination->initialize($config);
                $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
                $data['links'] = $this->pagination->create_links();

                $data['all_tickets'] =  $this->tickets->get_all_tickets_by_worker_id($this->session->userdata('worker_id'), $config["per_page"], $page);   //alle tickets in een variabele zetten


                $ticket = $this->tickets->get_ticket_info_by_id($id);

                $data['ticketdata']['campus'] = $ticket->campus;
                $data['ticketdata']['description'] = $ticket->description;
                $data['ticketdata']['subject'] = $ticket->subject;
                $data['ticketdata']['place'] = $ticket->place;
                $data['ticket_awaiting_count'] = $this->tickets->count_all_tickets_by_status_and_worker_id(1, $this->session->userdata['worker_id']);
                $data['ticket_in_progress_count'] = $this->tickets->count_all_tickets_by_status_and_worker_id(2, $this->session->userdata['worker_id']);
                $data['ticket_on_hold_count'] = $this->tickets->count_all_tickets_by_status_and_worker_id(3, $this->session->userdata['worker_id']);
                $data['ticket_solved_count'] = $this->tickets->count_all_tickets_by_status_and_worker_id(4, $this->session->userdata['worker_id']);
                $data['ticket_closed_count'] = $this->tickets->count_all_tickets_by_status_and_worker_id(5, $this->session->userdata['worker_id']);

                $this->load->view('workers/workers', $data);
            } else if (isset($selected)) {
                {
                    if (count($selected) > 1) {
                        $this->session->set_flashdata('error', $this->lang->line('controller_workers_error_edit_one_ticket'));
                        redirect('members/index');
                    }
                }
            }
        }else if ($this->input->post('sbm') == 'closed') {
            if (isset($selected)) {
                foreach ($selected as $id) {

                    $this->tickets->change_status($id, 5 );
                }
                $this->session->set_flashdata('message', 'Status changes succesfully.');

                redirect('members/index');
            } else if (isset($selected)) {
                {
                    if (count($selected) > 1) {
                        $this->session->set_flashdata('error', $this->lang->line('controller_workers_error_edit_one_ticket'));
                        redirect('members/index');
                    }
                }
            }
        } else {
            $this->session->set_flashdata('error', $this->lang->line('controller_workers_error_select_ticket'));

            redirect('members/index');
        }
    }



}