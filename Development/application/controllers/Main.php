<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Main extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	/**
	* Toegevoegd door: Koen Castermans
	* Datum: 5/5/2016
	* Reden: laden van language
	*/
	public function __construct()
	{
		parent::__construct();

		//taal bepalen
		$this->site_language->load_language();

	}

	/**
	 * @Author: Jonas Stams
	 * @Reviewer: Koen Castermans
	 */
	public function index()
	{
		$this->login();
	}

	/**
	 * @Author: Jonas Stams
	 * @Reviewer: Koen Castermans
	 */
	public function login(){
		$this->load->view('login');
	}

	/**
	 * @Author: Jonas Stams
	 * @Reviewer: Koen Castermans
	 */
	public function logout(){
		$this->session->sess_destroy();
		redirect('main./');
	}

	/**
	 * @Author: Jonas Stams
	 * @Reviewer: Koen Castermans
	 */
	public function restricted(){
		$this->load->view('restricted');
	}

	/**
	 * @Author: Jonas Stams
	 * @Reviewer: Koen Castermans
	 */
	public function login_validation(){
		$this->load->library('form_validation');  //Libraries->Session->Form_validation.php
		$this->load->model('workers');  //Libraries->Session->Form_validation.php
		$this->form_validation->set_rules('email', 'Email', 'required|trim|valid_email|callback_validate_credentials');
		$this->form_validation->set_rules('password', 'Password', 'required|md5|trim');

		if($this->form_validation->run()){
			$user = $this->users->get_user_by_email($this->input->post('email'));


			$data = array(
				'email' => $this->input->post('email'),
				'id' => $user->id,
				'username' => $user->username,
				'user_role' => $user->user_role,
				'is_logged_in' => 1
			);
			if($user->user_role==4){
				$data['worker_id'] = $this->workers->get_worker_id_by_user_id($user->id);
			}

			if($user->banned == 0){
				$this->session->set_userdata($data);
				redirect('members');
			}else{
				$data = array('message' => $this->lang->line('main_message_acc_blocked'));
				$this->load->view('login', $data);
			}

		}else{
			$this->load->view('login');
		}
	}

	/**
	 * @Author: Jonas Stams
	 * @Reviewer: Koen Castermans
	 */
	public function validate_credentials(){
		$this->load->model('users');

		if($this->users->can_log_in($this->input->post('email'), $this->input->post('password'))){
			return true;
		}else{
			$this->form_validation->set_message('validate_credentials', $this->lang->line('main_message_incorrect_email_password'));
			return false;
		}
	}

	/**
	 * @Author: Koen Castermans
	 * @Reviewer:
	 */
	public function set_language(){
		if($this->input->post('english')){
			$this->session->set_userdata('language', 'english');
			redirect('main');
		} else {
			$this->session->set_userdata('language', 'nederlands');
			redirect('main');
		}
	}


}
