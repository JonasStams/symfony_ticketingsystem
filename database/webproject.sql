-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Gegenereerd op: 27 mei 2016 om 17:19
-- Serverversie: 10.1.10-MariaDB
-- PHP-versie: 5.6.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `webproject`
--

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `campusses`
--

CREATE TABLE `campusses` (
  `id` int(11) NOT NULL,
  `description` varchar(80) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Gegevens worden geëxporteerd voor tabel `campusses`
--

INSERT INTO `campusses` (`id`, `description`) VALUES
(1, 'Diepenbeek'),
(2, 'Elfde Linie'),
(3, 'Guffenslaan'),
(4, 'Quartier Canal'),
(5, 'Vildersstraat');

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `campusses_worker`
--

CREATE TABLE `campusses_worker` (
  `worker` int(11) NOT NULL,
  `campus` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Gegevens worden geëxporteerd voor tabel `campusses_worker`
--

INSERT INTO `campusses_worker` (`worker`, `campus`) VALUES
(1, 1),
(1, 2);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `tickets`
--

CREATE TABLE `tickets` (
  `id` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `assigned_to` int(11) DEFAULT NULL,
  `assigned_by` int(11) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `subject` varchar(40) NOT NULL,
  `description` varchar(300) NOT NULL,
  `campus` int(11) NOT NULL,
  `place` varchar(20) NOT NULL,
  `priority` int(1) NOT NULL,
  `created_on` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `assigned_on` datetime NOT NULL,
  `closed_on` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Gegevens worden geëxporteerd voor tabel `tickets`
--

INSERT INTO `tickets` (`id`, `created_by`, `assigned_to`, `assigned_by`, `status`, `subject`, `description`, `campus`, `place`, `priority`, `created_on`, `assigned_on`, `closed_on`) VALUES
(49, 5, 1, 0, 3, 'Stinkende afvoerput', 'De afvoerput van lokaal EB225 stinkt abnormaal hard. Ik heb er daarnstrax al water ingegoten maar het probleem is nog niet opgelost. Gelieve dit na te kijken.', 1, 'EB225', 0, '2016-05-02 07:13:52', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(50, 5, 1, 0, 3, 'Diefstal kabel', 'Kan het zijn dat er een HDMI kabel is gestolen in lokaal EB 162. Ik wou daarstrax gebruik maken van de computer en merkte dat deze afwezig was. Op het secetariaat konden ze mij er geen andere bezorgen.', 2, 'lokaal EB 162', 2, '2016-05-02 08:15:03', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(52, 5, 1, 0, 2, 'Rookmelder vervangen', 'Er knippert een rode led op de rookmelder in gang 3 verdiep 5 van de studententoren. Kan het zijn dat dit wil zeggen dat de batterij bijna leeg is? ', 3, 'Studententoren', 1, '2016-05-02 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(53, 2, 1, 0, 3, 'Handdroger werkt niet', 'Het toestel om je handen te drogen op het tweede verdiep is kapot. Dit is de badkamer naast lokaal L 215. ', 2, 'L215', 0, '2016-05-03 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(54, 5, 1, 0, 3, 'kauwgom op tafel', 'Er plakt kauwgom onder verschillende tafel''s in klaslokalen EB210, EB222 en EB223. Gelieve dit te reinigen.', 3, 'EB210,EB222,EB223', 0, '2016-05-04 09:02:11', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(55, 1, 1, 0, 5, 'Computer blijft updaten', 'De computer in dit lokaal blijft updaten. Er lijkt geen verbetering in te komen. Zelfs na een heel uur lesgeven was het updaten nog niet klaar. Ik heb de computer na mijn les laten aanstaan. Hopelijk is het tegen de volgende keer verholpen.', 1, 'EB241', 0, '2016-05-04 12:23:32', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(63, 8, NULL, 0, 2, 'Stoel vervangen', 'Er is een stoel in het lokaal waarvan de schroeven vervangen mogen worden. De houten deel van de stoel zit los en maakt een verveld geluid indien hij gebruikt wordt. Ik heb hem links vooraan in de klas gezet. Vevang de stoel indien het herstellen niet meer zou lukken.', 1, 'EB101', 0, '2016-05-05 16:25:11', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(64, 9, NULL, 0, 1, 'Projector loopt warm', 'Beste\r\n\r\nVorige week maandag was ik aan het lesgeven in lokaal 121. Hierbij maakten we gebruik van de projector de oplossingen van de toets op het scherm te tonen. Na een half uur werken kwam er de melding: "Projector loopt warm, gelieve de filters uit te kuisen". Zeer vervelend!\r\n\r\ngroeten,\r\nMax ', 3, 'EB121', 1, '2016-04-05 14:13:25', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(66, 11, NULL, NULL, 1, 'Lamp kapot', 'Beste\r\n\r\nEr zijn in klaslokaal 215 meerdere lampen die niet meer werken.\r\nIs het mogelijk om deze te vervangen of om ze weg te nemen. Sommige flikkeren nog en dit stoort de leerlingen die in het lokaal les volgen.\r\n\r\n\r\nMet vriendelijke groeten,\r\nSara Wouters ', 4, 'L215', 0, '2016-03-14 13:23:38', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `ticket_messages`
--

CREATE TABLE `ticket_messages` (
  `id` int(11) NOT NULL,
  `ticket_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `message` text NOT NULL,
  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Gegevens worden geëxporteerd voor tabel `ticket_messages`
--

INSERT INTO `ticket_messages` (`id`, `ticket_id`, `user_id`, `message`, `created`) VALUES
(2, 53, 8, 'Status bijgewerkt', '2016-05-05 16:02:30'),
(3, 49, 8, 'Status bijgewerkt', '2016-05-05 18:01:56'),
(4, 52, 8, 'Status bijgewerkt', '2016-05-05 18:48:59'),
(5, 54, 8, 'Status bijgewerkt', '2016-05-05 18:48:59'),
(6, 52, 8, 'Status bijgewerkt', '0000-00-00 00:00:00'),
(7, 52, 8, 'Status bijgewerkt naar In progress', '0000-00-00 00:00:00'),
(8, 54, 8, 'Status bijgewerkt naar On hold', '0000-00-00 00:00:00'),
(9, 55, 8, 'Status bijgewerkt naar Closed', '0000-00-00 00:00:00'),
(10, 55, 8, 'Status bijgewerkt naar On hold', '0000-00-00 00:00:00'),
(11, 53, 8, 'Status bijgewerkt naar In progress', '0000-00-00 00:00:00'),
(12, 53, 8, 'Status bijgewerkt naar On hold', '0000-00-00 00:00:00'),
(13, 55, 8, 'Status bijgewerkt naar Closed', '2016-05-05 21:16:35'),
(14, 49, 8, 'Status bijgewerkt', '2016-05-05 21:17:14'),
(15, 63, 8, 'Status bijgewerkt naar In progress', '2016-05-06 11:09:08'),
(16, 63, 8, 'Status bijgewerkt', '2016-05-06 11:09:36');

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `ticket_status`
--

CREATE TABLE `ticket_status` (
  `id` int(11) NOT NULL,
  `description` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Gegevens worden geëxporteerd voor tabel `ticket_status`
--

INSERT INTO `ticket_status` (`id`, `description`) VALUES
(1, 'Awaiting operative'),
(2, 'In progress'),
(3, 'On hold'),
(4, 'Solved'),
(5, 'Closed');

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(50) COLLATE utf8_bin NOT NULL,
  `password` varchar(255) COLLATE utf8_bin NOT NULL,
  `email` varchar(100) COLLATE utf8_bin NOT NULL,
  `activated` tinyint(1) NOT NULL DEFAULT '1',
  `user_role` int(11) NOT NULL,
  `banned` tinyint(1) NOT NULL DEFAULT '0',
  `ban_reason` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `new_password_key` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `new_password_requested` datetime DEFAULT NULL,
  `new_email` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `new_email_key` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `last_ip` varchar(40) COLLATE utf8_bin NOT NULL,
  `last_login` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Gegevens worden geëxporteerd voor tabel `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `email`, `activated`, `user_role`, `banned`, `ban_reason`, `new_password_key`, `new_password_requested`, `new_email`, `new_email_key`, `last_ip`, `last_login`, `created`, `modified`) VALUES
(1, 'Jonas Hermans', '$2a$08$yy4Q0epVLo0CtjZTNuMbn.sokHmA4EBZgg/NYrYy3dnA4Nf9ma8pW', 'jonasstams@gmail.com', 1, 1, 0, NULL, NULL, NULL, NULL, NULL, '', '0000-00-00 00:00:00', '2016-03-30 12:06:07', '2016-05-27 14:00:56'),
(2, 'Pieter Franckx', '$2a$08$yy4Q0epVLo0CtjZTNuMbn.sokHmA4EBZgg/NYrYy3dnA4Nf9ma8pW', 'pieter.bollen@student.pxl.be', 1, 4, 0, NULL, NULL, NULL, NULL, NULL, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2016-05-27 14:01:03'),
(3, 'Koen Lefebre', '$2a$08$yy4Q0epVLo0CtjZTNuMbn.sokHmA4EBZgg/NYrYy3dnA4Nf9ma8pW', 'koen.Lefebre@student.pxl.be', 1, 1, 0, NULL, NULL, NULL, NULL, NULL, '', '0000-00-00 00:00:00', '2016-03-23 19:49:51', '2016-05-27 14:01:09'),
(5, 'Jesse Stuiver', '$2a$08$yy4Q0epVLo0CtjZTNuMbn.sokHmA4EBZgg/NYrYy3dnA4Nf9ma8pW', 'JesseStuiver@hotmail.com', 1, 3, 0, NULL, NULL, NULL, NULL, NULL, '', '0000-00-00 00:00:00', '2016-05-02 10:20:20', '2016-05-27 14:01:16'),
(8, 'Tom Vandendael', '$2a$08$yy4Q0epVLo0CtjZTNuMbn.sokHmA4EBZgg/NYrYy3dnA4Nf9ma8pW', 'Tom.Vandendael@wm.com', 1, 4, 0, NULL, NULL, NULL, NULL, NULL, '', '0000-00-00 00:00:00', '2016-05-05 09:19:30', '2016-05-27 14:01:23'),
(9, 'Max Geron', '$2a$08$yy4Q0epVLo0CtjZTNuMbn.sokHmA4EBZgg/NYrYy3dnA4Nf9ma8pW', 'Max.Geron@msnmail.com', 1, 3, 0, NULL, NULL, NULL, NULL, NULL, '', '0000-00-00 00:00:00', '2016-05-05 09:20:42', '2016-05-27 14:01:51'),
(10, 'Katrien Lenaerts', '$2a$08$yy4Q0epVLo0CtjZTNuMbn.sokHmA4EBZgg/NYrYy3dnA4Nf9ma8pW', 'werkman1@werkman1.com', 1, 4, 0, NULL, NULL, NULL, NULL, NULL, '', '0000-00-00 00:00:00', '2016-05-26 14:04:12', '2016-05-27 14:02:00'),
(11, 'Sara Wouters ', '$2a$08$yy4Q0epVLo0CtjZTNuMbn.sokHmA4EBZgg/NYrYy3dnA4Nf9ma8pW', 'SaraWouters@protonmail.com', 1, 4, 0, NULL, NULL, NULL, NULL, NULL, '', '0000-00-00 00:00:00', '2016-05-26 14:12:39', '2016-05-27 14:02:08'),
(12, 'Pieter Degraeve', '$2a$08$yy4Q0epVLo0CtjZTNuMbn.sokHmA4EBZgg/NYrYy3dnA4Nf9ma8pW', 'test.admin@student.pxl.be', 1, 1, 0, NULL, NULL, NULL, NULL, NULL, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2016-05-27 14:02:16'),
(13, 'Emma Devos', '$2a$08$yy4Q0epVLo0CtjZTNuMbn.sokHmA4EBZgg/NYrYy3dnA4Nf9ma8pW', 'dispatcher@student.pxl.be', 1, 3, 0, NULL, NULL, NULL, NULL, NULL, '', '0000-00-00 00:00:00', '2016-05-27 15:44:02', '2016-05-27 14:02:25'),
(14, 'Nele Vandenberge', '$2a$08$yy4Q0epVLo0CtjZTNuMbn.sokHmA4EBZgg/NYrYy3dnA4Nf9ma8pW', 'admin@student.pxl.be', 1, 1, 0, NULL, NULL, NULL, NULL, NULL, '', '0000-00-00 00:00:00', '2016-05-27 15:45:20', '2016-05-27 14:02:54'),
(15, 'Willem Celen', '$2a$08$yy4Q0epVLo0CtjZTNuMbn.sokHmA4EBZgg/NYrYy3dnA4Nf9ma8pW', 'admin2@student.pxl.be', 1, 1, 0, NULL, NULL, NULL, NULL, NULL, '', '0000-00-00 00:00:00', '2016-05-27 15:45:54', '2016-05-27 13:45:54'),
(16, 'Laura Verschelde', '$2a$08$GGhHXTKkrT/jH.PL27c4tejGTVMFsOqUK35pwScQZC71AQ0jS6cvy', 'admin3@student.pxl.be', 1, 1, 0, NULL, NULL, NULL, NULL, NULL, '', '0000-00-00 00:00:00', '2016-05-27 15:46:23', '2016-05-27 13:46:23'),
(17, 'Stef Breuls', '$2a$08$8WzP/RTE7VJsISX4ZSbv/OSY3qlEBvMj9tLyYl2qz292tpKgRaHO2', 'werkman@student.pxl.be', 1, 4, 0, NULL, NULL, NULL, NULL, NULL, '', '0000-00-00 00:00:00', '2016-05-27 15:55:16', '2016-05-27 13:55:16'),
(18, 'Marie Bogaerts', '$2a$08$yy4Q0epVLo0CtjZTNuMbn.sokHmA4EBZgg/NYrYy3dnA4Nf9ma8pW', 'werkman2@student.pxl.be', 1, 4, 0, NULL, NULL, NULL, NULL, NULL, '', '0000-00-00 00:00:00', '2016-05-27 15:55:57', '2016-05-27 14:02:33');

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `user_profiles`
--

CREATE TABLE `user_profiles` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `country` varchar(20) COLLATE utf8_bin DEFAULT NULL,
  `website` varchar(255) COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Gegevens worden geëxporteerd voor tabel `user_profiles`
--

INSERT INTO `user_profiles` (`id`, `user_id`, `country`, `website`) VALUES
(2, 8, NULL, NULL),
(3, 9, NULL, NULL),
(5, 5, NULL, NULL),
(8, 8, NULL, NULL),
(9, 9, NULL, NULL),
(10, 10, NULL, NULL);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `user_roles`
--

CREATE TABLE `user_roles` (
  `id` int(11) NOT NULL,
  `role` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Gegevens worden geëxporteerd voor tabel `user_roles`
--

INSERT INTO `user_roles` (`id`, `role`) VALUES
(1, 'admin'),
(2, 'user'),
(3, 'dispatcher'),
(4, 'werkman');

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `workers`
--

CREATE TABLE `workers` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `active` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Gegevens worden geëxporteerd voor tabel `workers`
--

INSERT INTO `workers` (`id`, `user_id`, `active`) VALUES
(1, 8, 1),
(2, 10, 1),
(3, 11, 1),
(4, 17, 1),
(5, 18, 1);

--
-- Indexen voor geëxporteerde tabellen
--

--
-- Indexen voor tabel `campusses`
--
ALTER TABLE `campusses`
  ADD PRIMARY KEY (`id`);

--
-- Indexen voor tabel `campusses_worker`
--
ALTER TABLE `campusses_worker`
  ADD PRIMARY KEY (`worker`,`campus`),
  ADD KEY `workerid` (`worker`),
  ADD KEY `campusid` (`campus`);

--
-- Indexen voor tabel `tickets`
--
ALTER TABLE `tickets`
  ADD PRIMARY KEY (`id`),
  ADD KEY `status` (`status`),
  ADD KEY `userid` (`created_by`),
  ADD KEY `campus` (`campus`),
  ADD KEY `werkmanid` (`assigned_to`),
  ADD KEY `assigned_by` (`assigned_by`);

--
-- Indexen voor tabel `ticket_messages`
--
ALTER TABLE `ticket_messages`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ticket_id` (`ticket_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexen voor tabel `ticket_status`
--
ALTER TABLE `ticket_status`
  ADD PRIMARY KEY (`id`);

--
-- Indexen voor tabel `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_role` (`user_role`);

--
-- Indexen voor tabel `user_profiles`
--
ALTER TABLE `user_profiles`
  ADD PRIMARY KEY (`id`);

--
-- Indexen voor tabel `user_roles`
--
ALTER TABLE `user_roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexen voor tabel `workers`
--
ALTER TABLE `workers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `userid` (`user_id`);

--
-- AUTO_INCREMENT voor geëxporteerde tabellen
--

--
-- AUTO_INCREMENT voor een tabel `campusses`
--
ALTER TABLE `campusses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT voor een tabel `tickets`
--
ALTER TABLE `tickets`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=67;
--
-- AUTO_INCREMENT voor een tabel `ticket_messages`
--
ALTER TABLE `ticket_messages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT voor een tabel `ticket_status`
--
ALTER TABLE `ticket_status`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT voor een tabel `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT voor een tabel `user_profiles`
--
ALTER TABLE `user_profiles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT voor een tabel `user_roles`
--
ALTER TABLE `user_roles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT voor een tabel `workers`
--
ALTER TABLE `workers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- Beperkingen voor geëxporteerde tabellen
--

--
-- Beperkingen voor tabel `campusses_worker`
--
ALTER TABLE `campusses_worker`
  ADD CONSTRAINT `campusses_worker_ibfk_1` FOREIGN KEY (`campus`) REFERENCES `campusses` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `campusses_worker_ibfk_2` FOREIGN KEY (`worker`) REFERENCES `workers` (`id`) ON DELETE CASCADE;

--
-- Beperkingen voor tabel `tickets`
--
ALTER TABLE `tickets`
  ADD CONSTRAINT `tickets_ibfk_1` FOREIGN KEY (`status`) REFERENCES `ticket_status` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `tickets_ibfk_2` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `tickets_ibfk_3` FOREIGN KEY (`campus`) REFERENCES `campusses` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `tickets_ibfk_4` FOREIGN KEY (`assigned_to`) REFERENCES `workers` (`id`) ON UPDATE CASCADE;

--
-- Beperkingen voor tabel `ticket_messages`
--
ALTER TABLE `ticket_messages`
  ADD CONSTRAINT `ticket_messages_ibfk_1` FOREIGN KEY (`ticket_id`) REFERENCES `tickets` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `ticket_messages_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Beperkingen voor tabel `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_ibfk_1` FOREIGN KEY (`user_role`) REFERENCES `user_roles` (`id`) ON UPDATE CASCADE;

--
-- Beperkingen voor tabel `workers`
--
ALTER TABLE `workers`
  ADD CONSTRAINT `workers_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
