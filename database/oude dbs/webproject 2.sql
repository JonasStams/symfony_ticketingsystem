-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Gegenereerd op: 04 mei 2016 om 17:03
-- Serverversie: 10.1.9-MariaDB
-- PHP-versie: 5.6.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `webproject`
--

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `campusses`
--

CREATE TABLE `campusses` (
  `id` int(11) NOT NULL,
  `description` varchar(80) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Gegevens worden geëxporteerd voor tabel `campusses`
--

INSERT INTO `campusses` (`id`, `description`) VALUES
(1, 'Diepenbeek'),
(2, 'Elfde Linie'),
(3, 'Guffenslaan'),
(4, 'Quartier Canal'),
(5, 'Vildersstraat');

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `campusses_worker`
--

CREATE TABLE `campusses_worker` (
  `workerid` int(11) NOT NULL,
  `campusid` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `ci_sessions`
--

CREATE TABLE `ci_sessions` (
  `session_id` varchar(40) COLLATE utf8_bin NOT NULL DEFAULT '0',
  `ip_address` varchar(16) COLLATE utf8_bin NOT NULL DEFAULT '0',
  `user_agent` varchar(150) COLLATE utf8_bin NOT NULL,
  `last_activity` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `user_data` text COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `login_attempts`
--

CREATE TABLE `login_attempts` (
  `id` int(11) NOT NULL,
  `ip_address` varchar(40) COLLATE utf8_bin NOT NULL,
  `login` varchar(50) COLLATE utf8_bin NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `tickets`
--

CREATE TABLE `tickets` (
  `ticketsid` int(11) NOT NULL,
  `werkmanid` int(11) NOT NULL,
  `userid` int(11) NOT NULL,
  `campus` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `plaats` varchar(20) NOT NULL,
  `beschrijving` varchar(300) NOT NULL,
  `prioriteit` int(1) NOT NULL,
  `onderwerp` varchar(40) NOT NULL,
  `created` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Gegevens worden geëxporteerd voor tabel `tickets`
--

INSERT INTO `tickets` (`ticketsid`, `werkmanid`, `userid`, `campus`, `status`, `plaats`, `beschrijving`, `prioriteit`, `onderwerp`, `created`) VALUES
(49, 1, 5, 3, 1, '2093', 'snow', 0, 'john', '2016-05-02'),
(50, 1, 5, 3, 1, 'jikml', 'jikm', 0, 'jklm', '2016-05-02'),
(52, 1, 5, 3, 1, 'Lokaal 201', 'Lamp van beamer is stuk ', 0, 'Lamp', '2016-05-02'),
(53, 1, 2, 2, 1, 'L32', 'ketel', 0, 'kop', '2016-05-03'),
(54, 1, 2, 3, 1, 'aardbijparadijs', '1 aardbij', 0, '2 aarbijen', '2016-05-04'),
(55, 1, 1, 1, 1, 'B241', 'kop kapot', 0, 'laptop gevallen', '2016-05-04');

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `ticket_status`
--

CREATE TABLE `ticket_status` (
  `id` int(11) NOT NULL,
  `description` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Gegevens worden geëxporteerd voor tabel `ticket_status`
--

INSERT INTO `ticket_status` (`id`, `description`) VALUES
(1, 'Awaiting operative'),
(2, 'In progress'),
(3, 'Solved'),
(4, 'On hold'),
(5, 'Closed');

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(50) COLLATE utf8_bin NOT NULL,
  `password` varchar(255) COLLATE utf8_bin NOT NULL,
  `email` varchar(100) COLLATE utf8_bin NOT NULL,
  `activated` tinyint(1) NOT NULL DEFAULT '1',
  `user_role` int(11) NOT NULL,
  `banned` tinyint(1) NOT NULL DEFAULT '0',
  `ban_reason` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `new_password_key` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `new_password_requested` datetime DEFAULT NULL,
  `new_email` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `new_email_key` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `last_ip` varchar(40) COLLATE utf8_bin NOT NULL,
  `last_login` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Gegevens worden geëxporteerd voor tabel `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `email`, `activated`, `user_role`, `banned`, `ban_reason`, `new_password_key`, `new_password_requested`, `new_email`, `new_email_key`, `last_ip`, `last_login`, `created`, `modified`) VALUES
(1, 'jonas', '$2a$10$hVZyEHTsCzqexP87evUe8O/9erC/rlG2Y0QWP4JnqjqYvQZFcPWgi', 'jonasstams@gmail.com', 1, 1, 0, NULL, NULL, NULL, NULL, NULL, '', '0000-00-00 00:00:00', '2016-03-30 12:06:07', '2016-03-30 10:06:07'),
(2, 'pieter', '$2a$10$mRg7rkYpA5YS5BzRaL2xO.yHf1TqSjU2gfLA6xBr4V4BTzBH7paFC', 'pieter.bollen@student.pxl.be', 1, 1, 0, NULL, NULL, NULL, NULL, NULL, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2016-05-01 20:38:35'),
(3, 'koen', '$2a$10$lNbXVob3.ghThjNM59/ix.YDV3buIhncAzcAF3SgBGLTrcH/w70uG', 'koen.carstermans2@student.pxl.be', 1, 1, 0, NULL, NULL, NULL, NULL, NULL, '', '0000-00-00 00:00:00', '2016-03-23 19:49:51', '2016-04-11 11:58:17'),
(5, 'john', '$2a$08$NX.e5dT9/xvU5bK0ownS0OKaz36NYNDfSHKwclXE8mh4ApJzIAepy', 'johnsnow@hotmail.com', 1, 2, 0, NULL, NULL, NULL, NULL, NULL, '', '0000-00-00 00:00:00', '2016-05-02 10:20:20', '2016-05-02 08:20:20');

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `user_autologin`
--

CREATE TABLE `user_autologin` (
  `key_id` char(32) COLLATE utf8_bin NOT NULL,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `user_agent` varchar(150) COLLATE utf8_bin NOT NULL,
  `last_ip` varchar(40) COLLATE utf8_bin NOT NULL,
  `last_login` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `user_profiles`
--

CREATE TABLE `user_profiles` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `country` varchar(20) COLLATE utf8_bin DEFAULT NULL,
  `website` varchar(255) COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Gegevens worden geëxporteerd voor tabel `user_profiles`
--

INSERT INTO `user_profiles` (`id`, `user_id`, `country`, `website`) VALUES
(2, 8, NULL, NULL),
(3, 9, NULL, NULL),
(5, 5, NULL, NULL);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `user_roles`
--

CREATE TABLE `user_roles` (
  `id` int(11) NOT NULL,
  `role` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Gegevens worden geëxporteerd voor tabel `user_roles`
--

INSERT INTO `user_roles` (`id`, `role`) VALUES
(1, 'admin'),
(2, 'user'),
(3, 'dispatcher'),
(4, 'werkman');

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `workers`
--

CREATE TABLE `workers` (
  `workerid` int(11) NOT NULL,
  `userid` int(11) NOT NULL,
  `active` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Gegevens worden geëxporteerd voor tabel `workers`
--

INSERT INTO `workers` (`workerid`, `userid`, `active`) VALUES
(1, 1, 1);

--
-- Indexen voor geëxporteerde tabellen
--

--
-- Indexen voor tabel `campusses`
--
ALTER TABLE `campusses`
  ADD PRIMARY KEY (`id`);

--
-- Indexen voor tabel `campusses_worker`
--
ALTER TABLE `campusses_worker`
  ADD PRIMARY KEY (`workerid`,`campusid`),
  ADD KEY `workerid` (`workerid`),
  ADD KEY `campusid` (`campusid`);

--
-- Indexen voor tabel `ci_sessions`
--
ALTER TABLE `ci_sessions`
  ADD PRIMARY KEY (`session_id`);

--
-- Indexen voor tabel `login_attempts`
--
ALTER TABLE `login_attempts`
  ADD PRIMARY KEY (`id`);

--
-- Indexen voor tabel `tickets`
--
ALTER TABLE `tickets`
  ADD PRIMARY KEY (`ticketsid`),
  ADD KEY `status` (`status`),
  ADD KEY `userid` (`userid`),
  ADD KEY `campus` (`campus`),
  ADD KEY `werkmanid` (`werkmanid`);

--
-- Indexen voor tabel `ticket_status`
--
ALTER TABLE `ticket_status`
  ADD PRIMARY KEY (`id`);

--
-- Indexen voor tabel `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_role` (`user_role`);

--
-- Indexen voor tabel `user_autologin`
--
ALTER TABLE `user_autologin`
  ADD PRIMARY KEY (`key_id`,`user_id`);

--
-- Indexen voor tabel `user_profiles`
--
ALTER TABLE `user_profiles`
  ADD PRIMARY KEY (`id`);

--
-- Indexen voor tabel `user_roles`
--
ALTER TABLE `user_roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexen voor tabel `workers`
--
ALTER TABLE `workers`
  ADD PRIMARY KEY (`workerid`),
  ADD KEY `userid` (`userid`);

--
-- AUTO_INCREMENT voor geëxporteerde tabellen
--

--
-- AUTO_INCREMENT voor een tabel `campusses`
--
ALTER TABLE `campusses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT voor een tabel `login_attempts`
--
ALTER TABLE `login_attempts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT voor een tabel `tickets`
--
ALTER TABLE `tickets`
  MODIFY `ticketsid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=56;
--
-- AUTO_INCREMENT voor een tabel `ticket_status`
--
ALTER TABLE `ticket_status`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT voor een tabel `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT voor een tabel `user_profiles`
--
ALTER TABLE `user_profiles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT voor een tabel `user_roles`
--
ALTER TABLE `user_roles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT voor een tabel `workers`
--
ALTER TABLE `workers`
  MODIFY `workerid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- Beperkingen voor geëxporteerde tabellen
--

--
-- Beperkingen voor tabel `campusses_worker`
--
ALTER TABLE `campusses_worker`
  ADD CONSTRAINT `campusses_worker_ibfk_1` FOREIGN KEY (`campusid`) REFERENCES `campusses` (`id`) ON DELETE CASCADE;

--
-- Beperkingen voor tabel `tickets`
--
ALTER TABLE `tickets`
  ADD CONSTRAINT `tickets_ibfk_1` FOREIGN KEY (`status`) REFERENCES `ticket_status` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `tickets_ibfk_2` FOREIGN KEY (`userid`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `tickets_ibfk_3` FOREIGN KEY (`campus`) REFERENCES `campusses` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `tickets_ibfk_4` FOREIGN KEY (`werkmanid`) REFERENCES `workers` (`workerid`) ON DELETE CASCADE;

--
-- Beperkingen voor tabel `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_ibfk_1` FOREIGN KEY (`user_role`) REFERENCES `user_roles` (`id`) ON UPDATE CASCADE;

--
-- Beperkingen voor tabel `workers`
--
ALTER TABLE `workers`
  ADD CONSTRAINT `workers_ibfk_1` FOREIGN KEY (`userid`) REFERENCES `users` (`id`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
